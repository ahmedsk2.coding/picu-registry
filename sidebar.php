<?php 
session_start();
require ('dbconnect.php');
$user_id = $_SESSION["member_id"];

$access_PICU_patients=[0,1,2,3];
$access_PICU_registry=[0,2,3];
$access_PICU_endorsement=[0,2,3,4];
$access_PICU_control=[0];

$formationSQL = "SELECT * FROM members WHERE member_id = '".$user_id."'";
$result1 = $mysqli->query($formationSQL);
$user = $result1 -> fetch_array(MYSQLI_ASSOC);

$username1=$user['member_name'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=1024"> -->
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>PICU | Dashboard</title>

 
   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
      <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    
  <!-- overlayScrollbars -->
  <!-- <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
	 <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>


.select2-container--default .select2-selection--multiple .select2-selection__rendered li {
    list-style: none;
    color: black;
    
}
.select2-container--default .select2-selection--single .select2-selection__rendered {

    line-height: 15px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    cursor: pointer;
    display: contents;
    font-weight: bold;
    margin-right: 2px;
}
.cancelBtn{
color: black;
}
input{
  text-align: center;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
}
textarea{
  width:100%;
  padding-left: 2%;
    padding-right: 2%;
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
}
label{
  width: 100%;
}
</style>	
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
    
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
    <a class="nav-item nav-link" data-widget="pushmenu" href="#" role="button">
      <li class="nav-item fas fa-bars">
        
        
        
      </li></a>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="dashboard.php" class="nav-link">Home</a>
      </li>
		<!--
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
	  //-->
    </ul>

    <!-- SEARCH FORM -->
	  <!--
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
//-->
	  
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="https://www.healthpro.ai/main/" class="brand-link">
      <img src="dist/img/logo.png" alt="healthpro.Ai" width="110%" class="brand-image elevation-1"
           style="opacity: 1">
    
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <p style="color: white;font-weight: bolder;text-transform: capitalize;font-size: large;" class="d-block"> <?php echo $username1;?> </p>
          <a href="profile.php" class="d-block"> Edit Profile </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item">
            <a href="dashboard.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="stats.php" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Analysis
              
              </p>
            </a>
          </li>
          -->
          
          <?php 
          if (in_array($user['position'],$access_PICU_patients)){
         ?>
		 <li class="nav-item">
            <a href="PICU-patients.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                PICU Patients
              </p>
            </a>
      </li>

      <?php }
       if (in_array($user['position'],$access_PICU_registry)){
      ?>
      
      <li class="nav-item">
            <a href="PICU-registry.php" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                PICU Registry
              
              </p>
            </a>
          </li>

          <?php } 
          if (in_array($user['position'],$access_PICU_registry)){
         ?>
		 <li class="nav-item">
            <a href="nursing-assignment.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Nursing Assignment
              </p>
            </a>
      </li>

      
          <?php }
       if (in_array($user['position'],$access_PICU_endorsement)){
      ?>

      <li class="nav-item">
            <a href="PICU-Endorsement.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                PICU Endorsement
              </p>
            </a>
          </li>
         
          <?php }
        if (in_array($user['position'],$access_PICU_control)){
          ?>
<li class="nav-item">
            <a href="control.php" class="nav-link">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Control Page
              </p>
            </a>
 </li>
 <?php }
    
          ?>
 
			 <li class="nav-item">
            <a href="logout.php" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


