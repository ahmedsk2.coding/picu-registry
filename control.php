<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');

    if (!in_array($user['position'],$access_PICU_control)){
    
        echo "
        <div class='content-wrapper'>
        
      
        <section class='content'>
        <div class='container-fluid'>  
        <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
        </div>
        </div>
        </section>
        </div>
        ";
        require 'footer.php';
    
        exit();
      }
?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>
 
    function sortTable(n) {
  var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < rows.length - 1; i++) { //Change i=0 if you have the header th a separate table.
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

function del(value){
    if(!confirm("Do you really want to delete this user?")) {
    return false;
  }
        var rowname= "row";
        rowname+=value;
        row = document.getElementById(rowname);
        var id = value;
        data = {id: id};
        $.post('PICU-users-delete.php', data, function(data){
        // $(parent).html(data);
        });
        row.style.display = "none";
        
    }
	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php
    $formationSQL = "SELECT * FROM members";
    $result1 = $mysqli->query($formationSQL);
    $members = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM position";
    $result1 = $mysqli->query($formationSQL);
    $position = $result1 -> fetch_all(MYSQLI_ASSOC);
	?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Control Dash</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Control Dash</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      
<div class="row">

<div id="procedures" class="col-md-6">

           <!-- /.info-box -->

           <div class="card">
             <div  class="card-header">
               <h3 class="card-title"><i class="fas fa-procedures text-info"></i> Add Procedures</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                 <form method="POST" id="addProcedures1">
                        <div class="row">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Procedure Name</label>
                                    <input class="form-control" type="text" name="Procedure" required>
                                </div>
                            </div>
                            
                        </div>

                        <div class="p-t-15">
                            <button class="btn btn-info" type="submit" name="submitProcedure" value="submit">Submit</button>
                        </div>
                    </form>
					<?php

					

  if(isset($_REQUEST['submitProcedure']))
    {
			$name = "";
	
	 if (empty($_POST['Procedure'])
	){ 
 // Setting error message
 echo '<p style="color: red;"><strong>Please, Fill all the form</strong></p>';
		 
 } else { 
     	
		$name = $_POST['Procedure'];
        $checkquery = "select * from procedures where procedurename='".$name."'";
	    $checkresult = $mysqli->query($checkquery);
	    
            	    if(mysqli_num_rows($checkresult)>0)
             {
              echo '<p style="color:red;"><span>Procedure is Already in the list..!!</span></p>';
             }

     else {
	  
                    	  $query =  "INSERT INTO procedures (procedurename) VALUES ('".$name."')";	 
                    
                    	 if ($mysqli->query($query) === TRUE) {
                     echo '<p style="color:green;"><span>Submitted successfully..!!</span></p>';
                     } else {
                     echo '<p style="color:red;"><span>Submission Failed..!!</span></p>';
                    // 	 echo("Error description: " . mysqli_error($mysqli));
                     }    
	
             }		
    }
}

?>
                 </div>
                 <!-- /.col -->
                 
                 <!-- /.col -->
               </div>
               <!-- /.row -->
             </div>
             <!-- /.card-body -->
            
             <!-- /.footer -->
           </div>
           <!-- /.card -->
</div>
           
<div id="complications" class="col-md-6">

           <!-- /.info-box -->

           <div class="card">
             <div  class="card-header">
               <h3 class="card-title"><i class="fas fa-diagnoses text-info"></i> Add Complications</h3>
             </div>
             <!-- /.card-header -->
             <div class="card-body">
               <div class="row">
                 <div class="col-md-12">
                 <form method="POST" id="addcomplications">
                        <div class="row">
                            <div class="col">
                                <div class="input-group">
                                    <label class="label">Complication Name</label>
                                    <input class="form-control" type="text" name="complication" required>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col">
                                <div style="height: 70%;" class="input-group">
                                <input  style="height: inherit; width: 10%;" type="checkbox" value="1" name="ass_procedure">    
                                <label style="width: 90%;" class="label">Associated Procedure Required</label>
                                    
                                </div>
                            </div>
                            
                        </div>

                        <div class="p-t-15">
                            <button class="btn btn-info" type="submit" name="submitcomplication" value="submit">Submit</button>
                        </div>
                    </form>
					<?php

					

  if(isset($_REQUEST['submitcomplication']))
    {
			$name = "";
	
	 if (empty($_POST['complication'])
	){ 
 // Setting error message
 echo '<p style="color: red;"><strong>Please, Fill all the form</strong></p>';
		 
 } else { 
     	
		$name = $_POST['complication'];
        $checkquery = "select * from complications where complication='".$name."'";
	    $checkresult = $mysqli->query($checkquery);
	    
            	    if(mysqli_num_rows($checkresult)>0)
             {
              echo '<p style="color:red;"><span>Complication is Already in the list..!!</span></p>';
             }

     else {
      $ass_procedure = $_POST['ass_procedure'];
      
                    	  $query =  "INSERT INTO complications (complication, require_associated_procedure ) VALUES ('".$name."', '".$ass_procedure."')";	 
                    
                    	 if ($mysqli->query($query) === TRUE) {
                     echo '<p style="color:green;"><span>Submitted successfully..!!</span></p>';
                     } else {
                     echo '<p style="color:red;"><span>Submission Failed..!!</span></p>';
                    // 	 echo("Error description: " . mysqli_error($mysqli));
                     }    
	
             }		
    }
}

?>
 
                 </div>
                 <!-- /.col -->
                 
                 <!-- /.col -->
               </div>
               <!-- /.row -->
             </div>
             <!-- /.card-body -->
            
             <!-- /.footer -->
           </div>
           <!-- /.card -->
</div>
           
          
</div> <!--row -->



<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Manage Users</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
<?php 

   ?>
                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                        <div  class="table-responsive"> 
                                        <input class="form-control" id="search" type="text" placeholder="Search..">
                        <table id="myTable" class="table table-sm " cellspacing="0" width="100%">
                <thead>
                  <tr>
                
                    <th class="th-sm" scope="col" >Full Name</th>
                    <th onclick="sortTable(0)" class="th-sm" scope="col" >Login Name <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th class="th-sm" scope="col" >Email</th>
                    <th class="th-sm" scope="col" >Position</th>
                    <th  onclick="sortTable(3)"  class="th-sm" scope="col" >Status <i class="fa fa-sort" aria-hidden="true"></i></th>
                    <th class="th-sm" scope="col" >Action</th>
                  </tr>
                </thead>
                <tbody id="searchtable">
              

                  <?php

                                                                         
usort($members, function($a, $b) {
  return $a['active'] <=> $b['active'];
});

// $decodedP=array();                       
            foreach($members as $s){
       echo " <tr class='eachrow' id='row".$s['member_id']."'>
     
      <td class='eachcol full_name'  scope='row' ><p>".$s['full_name']."</p>
      <input class='txtdata' type='hidden' name='member_id' id='member_id' value='".$s['member_id']."' style='text-align: center;' >
      </td>
      
      <td class='eachcol member_name'  scope='row' ><input class='txtdata'  name='member_name' id='member_name' value='".$s['member_name']."' style='text-align: center;' ></td>

      <td class='eachcol member_email'  scope='row' ><p>".$s['member_email']."</p></td>

     

      ";
 echo"
      <td class='eachcol position'><select class='txtdata position form-control' style='width: 100%;'  name='position'>
      ";
      foreach($position as $value)
  {
      if ($s['position'] == $value['id']){      

      $position_name= $value['position'];

    }elseif ($s['position'] ==0){
        $position_name= 'Administrator';
    }}
      echo '<option selected value="' . $s['position'] . '">'. $position_name .'</option>';
        foreach($position as $value)
  {


      echo '<option value="' . $value['id'] . '">'.  $value['position']. '</option>';
      
  }
  echo '<option  value="0">Administrator</option>';

      echo"
      </select></td>
      <td class='eachcol activate'>";
   if ($s['active'] == 1){
     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='activate' value='1' checked>
     ";
   }else{
     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='activate'>";
   }
   echo "<label for='activate' style=' display: contents; '> Active</label><br> 
   </td>

   <td class='eachcol id' scope='row' ><input class='txtdata' name='id' value='".$s['member_id']."' style='display: none;'>
   ";

   if ($position_name== 'Administrator'){
  }else{
   echo"
   <a class='btn btn-danger'  onclick='del(".$s['member_id'].")'  style='color: aliceblue;line-height: 2;padding: 0px 10%;margin: 2%;'>Delete</a>
   ";
  }

   echo"
      </td>
         </tr >
      ";
      // <textarea style='width: 100%;  padding-left: 5px;padding-right: 5px;'; oninput='auto_grow(this)' class='txtdata' name='procedures' value='".$s['PROCEDURES']."' >".$s['PROCEDURES']."</textarea>
      // onclick='discharge(".$s['ID'].")'
    // var_dump($decodedP) ;
    
    }
            // 

            ?>

         
  </tbody>
</table> 

</div>



           
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
 </div> <!--row -->
			
 

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

</div><!--/. container-fluid -->

    </section>
    <!-- /.content -->


<!-- PAGE SCRIPTS -->    


  </div>
  <!-- /.content-wrapper -->


<?php

require 'footer.php';

?>


<script>
$(document).ready(function($) {
  
  $('.txtdata').on('change', function(){
var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.full_name').find('input[name$="member_id"]').val();
    var membername = $(parent).find('.member_name').find('input[name$="member_name"]').val();
    // alert(id);
    var position = $(parent).find('.position').find('select').val();
    var activatebox = $(parent).find('.activate').find('input[name$="activate"]');
    if(activatebox.prop('checked') === true){
          var activate = 1;
        }else{
          var activate = 0;
        }

      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {id: id, membername:membername, position: position, activate: activate};
    $.post('PICU-users-update.php', data, function(data){
    //   $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});


document.forms.namedItem("addProcedures1").onsubmit = function(event){
  createCard(event);
}

document.forms.namedItem("addcomplications").onsubmit = function(event){
  createCard(event);
}

$(document).ready(function(){
  $("#search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#searchtable tr").filter(function() {
      $(this).toggle($(this).find('td:eq(0), td:eq(1), td:eq(2)').text().replace(/\s+/g, ' ').toLowerCase().indexOf(value) > -1)
    });
  });
});

</script>