<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
$wt=$_GET['wt'];
$ht=$_GET['ht'];
$age=$_GET['age'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>PICU | Emergency Medications</title>

 
   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
      <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    
  <!-- overlayScrollbars -->
  <!-- <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
	 <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    input
{
    background-color: transparent !important;
    
}
 body {
    width: 210mm;
    height: 297mm;
  background: rgb(204,204,204); 
  -webkit-print-color-adjust:exact;
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}




@media print {
 
 
.col-md-1 {width:8%;  float:left;}
.col-md-2 {width:16%; float:left;}
.col-md-3 {width:25%; float:left;}
.col-md-4 {width:33%; float:left;}
.col-md-5 {width:42%; float:left;}
.col-md-6 {width:50%; float:left;}
.col-md-7 {width:58%; float:left;}
.col-md-8 {width:66%; float:left;}
.col-md-9 {width:75%; float:left;}
.col-md-10{width:83%; float:left;}
.col-md-11{width:92%; float:left;}
.col-md-12{width:100%; float:left;}
.row{width:100%;}
}
</style>	
</head>

<body  onload="window.print()" class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper ">

    <div class="row" >
    <div class="col-md-6" style="text-align: center;">
        <img src="dist/img/EHC.jpg" style="width: 100%;">
        <p style="font-size: larger;font-weight: bolder;margin-top: 10px;margin-left: 5px;padding-top: 20%;height: 64%;border: solid 0.5px;border-radius: 50px;">Patient Sticker</p>
    </div>
    <div class="col-md-6" style="text-align: center;">
    <img src="dist/img/QCN.jpg" style="width: 100%;">
    <img src="dist/img/qch1.png" style="width: 75%;">
    <p style="font-size: x-large;font-weight: bolder;margin-top: 10px;margin-bottom: 10px;">EMERGENCY INTRAVENOUS MEDICATIONS</p>
    
    <table style="width: 98%;font-weight: bolder;">
      <tr>
        <td style="border: 2px solid;width: 30%;">WT(kg):</td>
        <td style="border: 2px solid;width: 20%;"><?php echo $wt; ?> </td>
        <td style="border: 2px solid;width: 30%;">Age (Years):</td>
        <td style="border: 2px solid;width: 20%;"><?php echo $age; ?> </td>
      </tr>
      <tr>
        <td style="border: 2px solid;width: 30%;">Height(CM):</td>
        <td style="border: 2px solid;width: 20%;"><?php echo $ht; ?> </td>
        <td style="border: 2px solid;width: 30%;">BSA(m2):</td>
        <td style="border: 2px solid;width: 20%;"><?php echo round(sqrt($wt*$ht/3600), 2); ?> </td>
      </tr>
    </table>
    </div>
    </div>

    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-12">
            <table id="details" border=1 cellspacing=0 cellpadding=0 style="    width: 100%; border-collapse:collapse;border:solid black 1.5pt;margin: 0 auto;">
                <tr >
                <td style="text-align: center;font-size: x-large;" colspan="6">
                <span ><strong>Emergency Medications</strong></span>
                </td>
                </tr>
                <tr>

                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">MEDICATION</span>
                </td>
                
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">CONCENTRATION</span>
                </td>
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">DOSE Mg/Kg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Max. Dose mg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (mg)</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (ml)</span>
                </td>

                </tr>
                
         
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Atropin</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.5 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.02</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.5</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.02;
                         if ($dose > 0.5){
                                $dose = 0.5;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/0.5) ,2); ?> ml
                              </div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Adenosine first dose</div></td>
                                <td rowspan='2' style="text-align: center;"><div style='height:14pt; overflow:hidden'>3 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.1</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>6</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.1;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 6){
                                $dose = 6;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/3) ,2); ?> ml
                              </div></td>
                                </tr>

                                
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Adenosine second dose</div></td>
                                
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>12</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.2;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 12){
                                $dose = 12;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/3) ,2); ?> ml
                              </div></td>
                                </tr>
                                

                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Amiodarone</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>50 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>5</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>300</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 5;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 300){
                                $dose = 300;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/50) ,2); ?> ml
                              </div></td>
                                </tr>

                                </tr>
                                <tr style='height:14pt; font-weight:bold'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Calcium Chloride 10%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>100 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>20</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2000</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 20;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 2000){
                                $dose = 2000;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/100) ,2); ?> ml
                              </div></td>
                                </tr>
                                
                                
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Calcium Gluconate 10%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>100 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>60</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2000</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 60;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 2000){
                                $dose = 2000;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/100) ,2); ?> ml
                              </div></td>
                                </tr>

                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Dexamethasone</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>4 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.6</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>16</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.6;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 16){
                                $dose = 16;
                              }
                              echo round($dose, 2); 
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/4) ,2); ?> ml
                              </div></td>
                                </tr>

                                <tr style='height:14pt; font-weight:bold'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Dextrose 10%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>100 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>500</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1000 mg/kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 500;
                          
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/100) ,2); ?> ml
                              </div></td>
                                </tr>

                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Dextrose 25%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>250 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>500</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1000 mg/kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 500;
                          
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/250) ,2); ?> ml
                              </div></td>
                                </tr>

                                <tr style='height:14pt; font-weight:bold'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>EPINEPHRINE 1:10,000</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.1 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.01</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.01;
                             if ($dose > 1){
                                $dose = 1;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/0.1) ,2); ?> ml
                              </div></td>
                                </tr>
                                
                                <tr>
                                  <td colspan="6" style='background:#fff2cc'>
                                  <div style='margin-left: 5px;height:14pt; overflow:hidden'>If use Epinephrine 1:1000 concentration daliute to total 10 ml Normal Saline before use for IV</div>
                                </td>
                                </tr>


                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Hydrocortisone</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>50 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>100</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 2;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 100){
                                $dose = 100;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/50) ,2); ?> ml
                              </div></td>
                                </tr>


                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Lidocain 2%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>20 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1</div></td>
                                <td style="text-align: center;background: #595959;"><div style='height:14pt; overflow:hidden'></div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 1;
                             
                              echo round($dose, 2); 
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/20) ,2); ?> ml
                              </div></td>
                                </tr>

                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Magnesium Sulfate 10%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>100 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>50</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2000</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 50;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose >2000){
                                $dose = 2000;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/100) ,2); ?> ml
                              </div></td>
                                </tr>


                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Sodium Bicarbonate 4.2%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.5 Meq/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1 Meq/kg</div></td>
                                <td style="text-align: center;background: #595959;"><div style='height:14pt; overflow:hidden'></div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 1;
                              
                              echo round($dose, 2);
                              ?> meq
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/0.5) ,2); ?> ml
                              </div></td>
                                </tr>

                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Sodium Bicarbonate 8.4%</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1 Meq/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1 Meq/kg</div></td>
                                <td style="text-align: center;background: #595959;"><div style='height:14pt; overflow:hidden'></div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 1;
                              
                              echo round($dose, 2);
                              ?> meq
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/1) ,2); ?> ml
                              </div></td>
                                </tr>

            </table>
        </div>


    </div>
    
    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-12">
            <table id="details" border=1 cellspacing=0 cellpadding=0 style="    width: 100%; border-collapse:collapse;border:solid black 1.5pt;margin: 0 auto;">
                <tr >
                <td style="text-align: center;font-size: x-large;" colspan="6">
                <span ><strong>ELECTRICAL THERAPY</strong></span>
                </td>
                </tr>
                <tr>

                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">Type</span>
                </td>
                
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">Indication</span>
                </td>
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">DOSE</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Joule/Kg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (joules)</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Unit</span>
                </td>

                </tr>
                
         
                                <tr style='height:14pt'>
                                <td rowspan="3" ><div style='height:14pt;margin-left: 5px; overflow:hidden'>CARDIOVERSION</div></td>
                                <td style="text-align: center;" rowspan="3" ><div style='height:14pt;margin-left: 5px; overflow:hidden'>SVT / VT with Pulse</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1st 0.5 Joule /kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.5</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.5;
                             
                              echo round($dose);
                              ?>
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Joules</div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                               
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2nd 1 Joule /kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 1;
                             
                              echo round($dose);
                              ?>
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Joules</div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                               
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>3rd 2 Joule /kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 2;
                             
                              echo round($dose);
                              ?>
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Joules</div></td>
                                
                                </tr>

                                <tr style='height:14pt'>
                                <td rowspan="3" ><div style='height:14pt;margin-left: 5px; overflow:hidden'>DEFIBRILATION</div></td>
                                <td style="text-align: center;" rowspan="3" ><div style='height:14pt;margin-left: 5px; overflow:hidden'>VF / VT without Pulse</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1st 2 Joule /kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 2;
                              if ($dose >200){
                                $dose = 200;
                              }
                              echo round($dose);
                              ?>
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Joules</div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                               
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2nd 4 Joule /kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>4</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 4;
                              if ($dose >200){
                                $dose = 200;
                              }
                              echo round($dose);
                              ?>
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Joules</div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                               
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Subsequent</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>>4</div></td>
                                <td style="text-align: center;font-weight: bold;"><div style='height:14pt; overflow:hidden'>10/kg or Max 200</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Joules</div></td>
                                
                                </tr>

            </table>
        </div>


    </div>
  
    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-12">
            <table id="details" border=1 cellspacing=0 cellpadding=0 style="    width: 100%; border-collapse:collapse;border:solid black 1.5pt;margin: 0 auto;">
                <tr >
                <td style="text-align: center;font-size: x-large;" colspan="6">
                <span ><strong>Rapid Sequance Intubation</strong></span>
                </td>
                </tr>
                <tr>

                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">MEDICATION</span>
                </td>
                
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">CONCENTRATION</span>
                </td>
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">DOSE</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Max. Dose</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (ml)</span>
                </td>

                </tr>
                
         
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Fentanyl</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>50 mcg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1 mcg/kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2 mcg/kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 1;
                             
                              echo round($dose, 2);
                              ?> mcg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/50) ,2); ?> ml
                              </div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Ketamine</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>10 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>1 mg/kg</div></td>
                                <td style="text-align: center;background: #595959;"><div style='height:14pt; overflow:hidden'></div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 1;
                              
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/10) ,2); ?> ml
                              </div></td>
                                </tr>

                                
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Midazolam</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>5 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.1 mg/kg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>5 mg</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.1;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 5){
                                $dose = 5;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/5) ,2); ?> ml
                              </div></td>
                                </tr>
                                

                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Propofol</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>10 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2 mg/kg</div></td>
                                <td style="text-align: center;background: #595959;"><div style='height:14pt; overflow:hidden'></div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 2;
                             
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/10) ,2); ?> ml
                              </div></td>
                                </tr>

                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Rocuronium</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>10 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.6 mg/kg</div></td>
                                <td style="text-align: center;background: #595959;"><div style='height:14pt; overflow:hidden'></div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.6;
                              
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/10) ,2); ?> ml
                              </div></td>
                                </tr>
                                
                                
                                <tr style='height:14pt;border-top: double;font-weight: bold;'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>ETT Size and Depth</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Cuffed Tube</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                               <?php
                               if (is_numeric($age)){
                                  if ($age > 12){
                                      echo "6.5 - 8";

                                  }else{
                                    echo floor((($age/4)+3.5)*2)/2 . " - " . ceil((($age/4)+3.5)*2)/2;

                                  }
                                }else{
                                  echo "3.5 - 4.5";
                                }
                               ?>
                                
                              
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>Un-Cuffed Tube</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              if (is_numeric($age)){
                                  if ($age > 12){
                                    echo "N/A";

                                }else{
                                  echo floor((($age/4)+4)*2)/2 . " - " . ceil((($age/4)+4)*2)/2;

                                }

                            }else{
                              echo "3.5 - 4.5";
                            }
                              ?>
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                                <?php 
                              if (is_numeric($age)){
                                    if ($age > 12){
                                      echo "21 - 23 Cm";

                                  }else{
                                    echo round((($age/4)+3.5)*3) . " - " . round((($age/4)+4)*3) ." CM";

                                  }
                                }else{
                                  echo "10 - 13 Cm";
                                }  
                              ?>
                              </div></td>
                                </tr>


            </table>
        </div>


    </div>

    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-12">
            <table id="details" border=1 cellspacing=0 cellpadding=0 style="    width: 100%; border-collapse:collapse;border:solid black 1.5pt;margin: 0 auto;">
                <tr >
                <td style="text-align: center;font-size: x-large;" colspan="6">
                <span ><strong>Epilepsy Medication</strong></span>
                </td>
                </tr>
                <tr>

                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">MEDICATION</span>
                </td>
                
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">CONCENTRATION</span>
                </td>
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">DOSE Mg/Kg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Max. Dose Mg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (mg)</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (ml)</span>
                </td>

                </tr>
                
         
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Diazepam</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>5 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>10</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.2;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 10){
                                $dose = 10;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/5) ,2); ?> ml
                              </div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Lorazepam</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.1</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>4</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.1;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 4){
                                $dose = 4;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/2) ,2); ?> ml
                              </div></td>
                                </tr>

                                


            </table>
        </div>


    </div>

    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-12">
            <table id="details" border=1 cellspacing=0 cellpadding=0 style="    width: 100%; border-collapse:collapse;border:solid black 1.5pt;margin: 0 auto;">
                <tr >
                <td style="text-align: center;font-size: x-large;" colspan="6">
                <span ><strong>Anti-Dotes</strong></span>
                </td>
                </tr>
                <tr>

                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">MEDICATION</span>
                </td>
                
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">CONCENTRATION</span>
                </td>
                <td style=" text-align: center;  background:#8497B0; ">
                <span style="color: white;">DOSE Mg/Kg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Max. Dose Mg</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (mg)</span>
                </td>
                <td style=" text-align: center;  background:#002060; ">
                <span style="color: white;">Final Dose (ml)</span>
                </td>

                </tr>
                
         
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Flumazenil</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.1 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.01</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.01;
                           if ($dose > 0.2){
                                $dose = 0.2;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/0.1) ,2); ?> ml
                              </div></td>
                                
                                </tr>
                                <tr style='height:14pt'>
                                <td><div style='height:14pt;margin-left: 5px; overflow:hidden'>Naloxone</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.4 mg/ml</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>0.1</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>2</div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php 
                              $dose = $wt * 0.1;
                              if ($dose < 0.1){
                                $dose = 0.1;
                              } elseif ($dose > 2){
                                $dose = 2;
                              }
                              echo round($dose, 2);
                              ?> mg
                              </div></td>
                                <td style="text-align: center;"><div style='height:14pt; overflow:hidden'>
                              <?php echo round(($dose/0.4) ,2); ?> ml
                              </div></td>
                                </tr>

                                


            </table>
        </div>


    </div>
<!-- ./wrapper -->
<p style="margin-left:15px">Referances: 1) PALS 2020 medication guied, 2) IBM Micromedex NeoFax and Pediatric Drug, 3) Uptodate</p>
</div>


</body>

</html>
