<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');
  if (!in_array($user['position'],$access_PICU_endorsement)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }
?>

<style>
#d>a:first-child {
  color: lime;
  background-color: #28a745;
  padding: 5px;
}
</style>
   

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php

		$formationSQL = "SELECT * FROM endorsement";
		$result1 = $mysqli->query($formationSQL);
		$dates = $result1 -> fetch_all(MYSQLI_ASSOC);

    usort($dates, function($a, $b) {
      return $b['Dates']<=>$a['Dates'];
    });
    
		
function getdata($array, $key,$return, $val) {
    foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
            return $item[$return];
    return false;
}

			?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">PICU Endorsement</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">PICU Endorsement</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

<div class="row">

 <div class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> PICU Endorsement</h3>

 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">

                                        <div id='d'> 
                      

                  <?php

                                                                         

                                     
            foreach($dates as $s){

                echo "<a  type='button' class='btn btn-secondary'  style='margin: 10px;color: aliceblue; line-height: 2;padding: 0px 15px;' href='picu-endorsement-patients.php?date=".$s['Dates']."'>".$s['Dates']."</a>";
                

            }
            // 

            ?>

</div>
                  
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
 </div> <!--row -->
			
 

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

<!-- PAGE SCRIPTS -->


<?php
	



?>
</div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    


  </div>
  <!-- /.content-wrapper -->
<?php
require 'footer.php';
?>


