<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
$date = $_GET['date'];
$date1 = $date;
$date1 = date("Y-m-d", strtotime($date1));
// $passdate=$date1->format('Y-m-d');
if (!isset($date)){
  header("Location: PICU-patients.php");
}

	require ('dbconnect.php');
   
    date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

//get current admitted patients
    $formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL";
    $result1 = $mysqli->query($formationSQL);
    $activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

   

    $patientlist=array();
    
    foreach($activepicupatints as $s){

        $birthdate=$s['birthdate'];
        $today = date("Y-m-d");
        $diff = date_diff(date_create($birthdate), date_create($today));
        $age =$diff->format('%y');
        if ($age < 1){
            $age =$diff->format('%m');
            if($age < 1){
                $age =$diff->format('%d');
                $age.=' D';
            }
            else{
            $age.=' M';
            }
        } else {
            $age.=' Y';
        }
        array_push($patientlist,['id'=>$s['ID'], 'bed'=>$s['BED'],  'mrn'=>$s['MRN'],'name'=>$s['PNAME'],'age'=>$age,'w'=>'','m'=>'','day'=>'','night'=>'']);
    }
 
    
    
 // get yesterdays data from nurse endorcement DB
$formationSQL = "SELECT * FROM nursehandover order by date DESC limit 1";
$result1 = $mysqli->query($formationSQL);
$yesterdaysdata = $result1 -> fetch_array(MYSQLI_ASSOC);

$oldpatientlist=json_decode($yesterdaysdata['patientlist'], true);
if (is_null($yesterdaysdata['rest'])){
    $oldnurselistencoded=json_encode(
        [
            "ddf" => "", 
            "ddr" => "", 
            "dec" => "", 
            "dna" => "", 
            "dnm" => "", 
            "dsn" => "", 
            "mcr" => "", 
            "ndf" => "", 
            "ndr" => "", 
            "nna" => "", 
            "nnm" => "", 
            "nsn" => "", 
            "ocr" => "", 
            "cssd" => "", 
            "dpfc" => "", 
            "dpnr" => "", 
            "npnr" => "", 
            "nprm" => "", 
            "dpull" => "", 
            "npull" => "" 
         ] 
    );
// echo "ahmed";
}else{
    $oldnurselist=json_decode($yesterdaysdata['rest'], true);
    $oldnurselistencoded=$yesterdaysdata['rest'];
    // print_r(json_decode($yesterdaysdata['rest']));
}



//get todays data
$formationSQL = "SELECT * FROM nursehandover where date='$date1'";
$result1 = $mysqli->query($formationSQL);
$todaysdata = $result1 -> fetch_array(MYSQLI_ASSOC);

//   
//  var_dump( $oldpatientlist);
// var_dump( $oldnurselist);

// compare the 2 and make a new list


    $templist=array();

    function whatever($array, $key, $val) {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val)
                return  true;
        return false;
    }

    
    if ($todaysdata == null){}else{
foreach($oldpatientlist as $old){
    if (whatever($patientlist,'id',$old['id']) == true){
        // array_push($templist,$old);
        // cehange bed number as per new bed
        $key = array_search($old['id'], array_column($patientlist, 'id'));
        $old['bed']=$patientlist[$key]['bed'];

        // insert in the list
        $templist[$old['bed']]=$old; 
    } 
}
    }


foreach($patientlist as $list){

    if (whatever($templist,'id',$list['id']) == false){
// array_push($templist,$list);
$templist[$list['bed']]=$list;
}
}
    // var_dump( $templist);
   $newpatientlist= json_encode($templist);
// var_dump( $newpatientlist);

// update database by the new list of the day

if ($todaysdata == null AND $date1 == $today){
    $sql = "INSERT nursehandover SET patientlist='".$newpatientlist."',rest='".mysqli_real_escape_string($mysqli,$oldnurselistencoded)."', date='$date1'";
   
    if ($mysqli->query($sql) === TRUE) {
      $message= "Record updated successfully";
    } else {
     $message= "Error updating record: " . $mysqli->error;
    }
    // echo $message;

}elseif ($todaysdata !== null AND $date1 == $today){

 $sql = "UPDATE nursehandover SET patientlist='".$newpatientlist."' WHERE date='$date1'";
   
                 if ($mysqli->query($sql) === TRUE) {
                   $message= "Record updated successfully";
                 } else {
                  $message= "Error updating record: " . $mysqli->error;
                 }
                }
//  echo "$message";
// echo $date1;
$formationSQL = "SELECT * FROM nursehandover WHERE date='$date1'";
$result1 = $mysqli->query($formationSQL);
$nurselist = $result1 -> fetch_array(MYSQLI_ASSOC);

if ($nurselist == null){
    // header("Location: PICU-patients.php");

}

// var_dump($nurselist);
 // remaining as when changing the table to update the patient list array and insert again in mysql
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>PICU | Nurses Assignment</title>

 
   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
      <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    
  <!-- overlayScrollbars -->
  <!-- <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
	 <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    input
{
    background-color: transparent !important;
    
}
body {
  background: rgb(204,204,204); 
  -webkit-print-color-adjust:exact;
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
@media print {
  body, page {
    margin: 0;
    size: landscape;
    box-shadow: 0;
    
  }
  @page {size: A4 landscape; }
}
</style>	
</head>
<page size="A4" layout="landscape">
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">

    <div class="row" >

        <div class=WordSection1>

            <p class=MsoNormal align=center style='margin-top:3.35pt;margin-right:268.65pt;
            margin-bottom:0in;margin-left:265.6pt;text-align:center;line-height:16.55pt;
            '>

            <span style='position:
            absolute;z-index:251659264;left:0px;margin-left:877px;margin-top:9px;
            width:227px;height:62px'><img width=227 height=62
            src="dist/img/QCH.jpg"></span>

            <span ><b><span style='font-size:13.0pt;font-family:"Palatino Linotype",serif;
            '>QATIF HOSPITAL - NURSING DEPARTMENT</span></b></span><b></p>

            <p style='margin-top:0in;margin-right:268.65pt;
            margin-bottom:0in;margin-left:265.55pt;text-align:center;line-height:16.55pt;'><b><span
            style='font-size:13.0pt;font-family:"Palatino Linotype",serif;'>Nursing Daily Assignment Sheet</span></p>

            <p style='text-align:center;line-height:16.55pt;'><b><span
            style='font-size:11.0pt;font-family:"Palatino Linotype",serif;'>DEPARTMENT: PICU 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DAY:
            <!-- <select >
            <Option>Sunday</Option>
            <Option>Monday</Option>
            <Option>Tuesday</Option>
            <Option>Wednesday</Option>
            <Option>Thursday</Option>
            <Option>Friday</Option>
            <Option>Saturday</Option>
            </select> -->
            <?php
            date_default_timezone_set('Asia/Riyadh');
            $date=$date1;
            echo date('l', strtotime($date));
            ?>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DATE: <?php echo $date1; ?>
            </span>

            </p>
        </div>
    </div>

    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-6">
            <table id="details" border=1 cellspacing=0 cellpadding=0 style="border-collapse:collapse;border:solid black 0.50pt;margin: 0 auto;">
                <tr >
                <td width=41 rowspan=2  style=" width:44.4pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>RM#</strong></span></br>
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>Bed#</strong></span>
                </td>
                
                <td width=59 rowspan=2  style=" width:44.4pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>MR#</strong></span>
                </td>
                
                <td width=194 rowspan=2  style=" width:145.8pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>PATIENT'S NAME</strong></span>
                </td>

                <td width=40 rowspan=2  style=" width:29.8pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>Age</strong></span>
                </td>

                <td width=29 rowspan=2  style=" width:21.9pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>W</strong></span>
                </td>
                

                <td width=29 rowspan=2  style=" width:21.9pt; text-align: center; background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>M</strong></span>
                </td>

                <td width=146 colspan=2 style='width:109.75pt; text-align: center; background:#D75D8F; '>
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>Assigned Nurse</strong></span>
                </td>
                </tr>
                <tr >
                <td width=72  style="  text-align: center; background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt'><strong>Day</strong></span>
                </td>
                <td width=72  style="  text-align: center; background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt'><strong>Night</strong></span>
                </td>
                </tr>
                <?php
                $n = 0;
                
                $data=json_decode($nurselist['patientlist'], true);
                // var_dump($data);
                foreach($data as $nursel){
                    // var_dump($nursel);
                    
            
                    $n++;
                    // var_dump($nursel);
                    echo "
                    <tr style='height:14pt;font-weight: normal;overflow:hidden' class='eachrow'>
                    <td  style='display:none;'><div style='height:14pt; overflow:hidden'>".
                    $nursel['id']
                    ."</div></td>
                    <td><div style='height:14pt; overflow:hidden'>".
                    $nursel['bed']
                    ."</div></td>
                    <td><div style='height:14pt; overflow:hidden'>".
                    $nursel['mrn']
                    ."</div></td>
                    <td><div style='height:14pt; overflow:hidden'>".
                    $nursel['name']
                    ."</div></td>
                    <td><div style='height:14pt; overflow:hidden'>".
                    $nursel['age']
                    ."</div></td>
                    <td class='eachcol' ><div class='eachcolsub w' style='height:14pt; overflow:hidden'>";
                        if ($nursel['w'] =="yes"){
                            echo"
                            <input class=' txtdata'type='checkbox' name='w' id='w'  style='text-align: center;' checked>";
                        }else{
                            echo"
                            <input class=' txtdata'type='checkbox' name='w' id='w'  value='' style='text-align: center;' >";
                        }
                        
                    echo"
                  
                    </div></td>
                    <td class='eachcol' ><div class='eachcolsub m' style='height:14pt; overflow:hidden'>
                    <input class='txtdata' name='m' id='m' value='".$nursel['m']."' style='text-align: center;' >
                    </div></td>
                    <td class='eachcol' ><div class='eachcolsub day' style='height:14pt; overflow:hidden'>
                    <input class='txtdata' name='day' id='day' value='".$nursel['day']."' style='text-align: center;' >
                    </div></td>
                    <td class='eachcol' ><div class='eachcolsub night' style='height:14pt; overflow:hidden'>
                    <input class='txtdata' name='night' id='night' value='".$nursel['night']."' style='text-align: center;' >
                    </div></td>
                    </tr>
                    ";
                }

                for ($k = $n; $k < 15; $k++){
                    echo "
                                <tr style='height:14pt'>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                <td><div style='height:14pt; overflow:hidden'></div></td>
                                </tr>
                                ";
                            }
                ?>
            </table>
        </div>
        <div class="col-md-6">
            <table border=1 cellspacing=0 cellpadding=0 style="border-collapse:collapse;border:solid black 0.50pt;margin: 0 auto;">
                <tr >
                <td width=41 rowspan=2  style=" width:44.4pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>RM#</strong></span></br>
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>Bed#</strong></span>
                </td>
                
                <td width=59 rowspan=2  style=" width:44.4pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>MR#</strong></span>
                </td>
                
                <td width=194 rowspan=2  style=" width:145.8pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>PATIENT'S NAME</strong></span>
                </td>

                <td width=40 rowspan=2  style=" width:29.8pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>Age</strong></span>
                </td>

                <td width=29 rowspan=2  style=" width:21.9pt; text-align: center;  background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>W</strong></span>
                </td>
                

                <td width=29 rowspan=2  style=" width:21.9pt; text-align: center; background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>M</strong></span>
                </td>

                <td width=146 colspan=2 style='width:109.75pt; text-align: center; background:#D75D8F; '>
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif; line-height:13.15pt'><strong>Assigned Nurse</strong></span>
                </td>
                </tr>
                <tr>
                <td width=72  style="  text-align: center; background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt'><strong>Day</strong></span>
                </td>
                <td width=72  style="  text-align: center; background:#D75D8F; ">
                <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt'><strong>Night</strong></span>
                </td>
                </tr>
                <?php
                    for ($k = 0; $k < 15; $k++){
                        echo "
                        <tr style='height:14pt'>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        <td><div style='height:14pt; overflow:hidden'></div></td>
                        </tr>
                                    ";
                                }
                    ?>
            </table>
        </div>


    </div>
    <div class="row" style=" margin: 10px 0px 0px 0px; ">
    <div class="col-md-12">                  
    <?php $nursedata=json_decode($nurselist['rest'], true);
    // echo $date1; 
    // var_dump($nursedata);?>
        <table  border=1 cellspacing=0 cellpadding=0  style="width: 100%; border:solid black 1.0pt;"> 
        <tr style='height:26.5pt'>
        <td width=103    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Extra Unit Assignment</strong></span>
        </td>
        <td width=93    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Staff Name</strong></span>
        </td>
        <td width=101    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Extra Unit Assignment</strong></span>     
        </td>
        <td width=95    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Staff Name</strong></span>
        </td>
        <td width=89    style=" background:#4DC7E4; text-align: center; ">
             <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Position</strong></span>       
        </td>
        <td width=180    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Morning</strong></span>        
        </td>
        <td width=180    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Night</strong></span>
        </td>
        <td width=76    style=" background:#4DC7E4; text-align: center; ">
             <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Position</strong></span>       
        </td>
        <td width=84    style=" background:#4DC7E4; text-align: center; ">
            <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Morning</strong></span>
        </td>
        <td width=81    style=" background:#4DC7E4; text-align: center; ">
             <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;line-height:13.15pt;display: block'><strong>Night</strong></span>       
        </td>
        </tr>
        <tr>
        <td width=103 style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Discharge Patient File Checking</span>
        </td>
        <td width=93   style="   padding-left: 5px; padding-right: 3px;"  >
             <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
            <?php  echo "<input class='txtdata' name='dpfc' id='dpfc' value='".$nursedata['dpfc']."' style='text-align: left;' >"; ?>
            </span>

        </td>
        <td width=101    style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Negative Pressure Room Monitoring.</span>

        </td>
        <td width=95    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
        <?php  echo "<input class='txtdata' name='nprm' id='nprm' value='".$nursedata['nprm']."' style='text-align: left;' >"; ?>
    </span>

        </td>
        <td width=89    style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Nurse Manager/ Charge Nurse</span>

        </td>
        <td width=180    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
        <?php  echo "<input class='txtdata' name='dnm' id='dnm' value='".$nursedata['dnm']."' style='text-align: left;' >"; ?>
    </span>

        </td>
        <td width=180    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
        <?php  echo "<input class='txtdata' name='nnm' id='nnm' value='".$nursedata['nnm']."' style='text-align: left;' >"; ?>        
    </span>

        </td>
        <td width=76    style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Discharge Facilitator</span>

        </td>
        <td width=84    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
        <?php  echo "<input class='txtdata' name='ddf' id='ddf' value='".$nursedata['ddf']."' style='text-align: left;' >"; ?> 
    </span>
        
        </td>
        <td width=81    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
        <?php  echo "<input class='txtdata' name='ndf' id='ndf' value='".$nursedata['ndf']."' style='text-align: left;' >"; ?> 
    </span>
        
        </td>
        </tr>
        <tr style=' height:67.8pt'>
        <td width=103 rowspan=2 style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Oxygen Checking & Refill(in E-Cart should be more than 1000)</span>
        </td>
        <td width=93 rowspan=2  style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='ocr' id='ocr' value='".$nursedata['ocr']."' 
             style='text-align: left;height: 175px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['ocr']."</textarea>"; ?> 
            </span>

        </td>
        <td width=101  rowspan=2  style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">CSSD Equipment checked</span>

        </td>
        <td width=95  rowspan=2  style="   padding-left: 5px; padding-right: 3px;"  >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='cssd' id='cssd' value='".$nursedata['cssd']."' 
             style='text-align: left;height: 175px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['cssd']."</textarea>"; ?> 
            </span>

        </td>
        <td width=89  rowspan=2  style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Staff Nurse</span>

        </td>
        <td width=180  rowspan=2  style="   padding-left: 5px; padding-right: 3px;"  >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;"class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='dsn' id='dsn' value='".$nursedata['dsn']."' 
             style='text-align: left;height: 175px;width: 160px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['dsn']."</textarea>"; ?> 
            </span>

        </td>
        <td width=180 rowspan=2   style="   padding-left: 5px; padding-right: 3px;"  >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='nsn' id='nsn' value='".$nursedata['nsn']."' 
             style='text-align: left;height: 175px;width: 160px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['nsn']."</textarea>"; ?> 
            </span>

        </td>
        <td width=76    style="   padding-left: 5px; padding-right: 3px;" >
             <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Patient Needs responder Team (PNR)</span>

        </td>
        <td width=84    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='dpnr' id='dpnr' value='".$nursedata['dpnr']."' 
             style='text-align: left;height: 80px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['dpnr']."</textarea>"; ?> 
            </span>
        
        </td>
        <td width=81    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='npnr' id='npnr' value='".$nursedata['npnr']."' 
             style='text-align: left;height: 80px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['npnr']."</textarea>"; ?> 
            </span>
        
        </td>
        </tr>
        <tr style=' height:67.8pt'>
        <td width=76    style="   padding-left: 5px; padding-right: 3px; background:#29C2E3">
            <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Disaster Nurse</span>
        </td>
        <td width=84    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='ddr' id='ddr' value='".$nursedata['ddr']."' 
             style='text-align: left;height: 80px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['ddr']."</textarea>"; ?> 
            </span>
        </td>
        <td width=81    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='ndr' id='ndr' value='".$nursedata['ndr']."' 
             style='text-align: left;height: 80px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['ndr']."</textarea>"; ?> 
            </span>
        </td>
        </tr>
        <tr style='height:34.0pt'>
        <td width=103    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Medication & Store Room (Tem., Expi- ry, Etc.)</span>
        
        </td>
        <td width=93    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='mcr' id='mcr' value='".$nursedata['mcr']."' 
             style='text-align: left;height: 44px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['mcr']."</textarea>"; ?> 
            </span>

        </td>
        <td width=101   style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Daily Equipment Checking</span>

        </td>
        <td width=95    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='dec' id='dec' value='".$nursedata['dec']."' 
             style='text-align: left;height: 44px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['dec']."</textarea>"; ?> 
            </span>
        </td>
        <td width=89    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Nurse Aid</span>

        </td>
        <td width=180    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='dna' id='dna' value='".$nursedata['dna']."' 
             style='text-align: left;height: 44px;width: 160px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['dna']."</textarea>"; ?> 
            </span>
        
        </td>
        <td width=180    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='nna' id='nna' value='".$nursedata['nna']."' 
             style='text-align: left;height: 44px;width: 160px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['nna']."</textarea>"; ?> 
            </span>

        </td>
        <td width=76    style="   padding-left: 5px; padding-right: 3px; background:#29C2E3">
            <span style="font-size: 8pt;font-weight: normal;font-family: arial;display: block;">Staff Nurse – Due For Pullout</span>
        
        </td>
        <td width=84    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='dpull' id='dpull' value='".$nursedata['dpull']."' 
             style='text-align: left;height: 44px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['dpull']."</textarea>"; ?> 
            </span>

        </td>
        <td width=81    style="   padding-left: 5px; padding-right: 3px;" >
        <span style="font-size: 8pt;font-weight: normal;font-family: times new roman;" class='eachcolsub '>
             <?php  echo "<textarea class='txtdata' name='npull' id='npull' value='".$nursedata['npull']."' 
             style='text-align: left;height: 44px;width: 84px;font-size: 11pt;font-weight: normal;border: 0;resize: none;font-family: times new roman;' >".$nursedata['npull']."</textarea>"; ?> 
            </span>

        </td>
        </tr>
        </table>

    </div>
    </div>
  
    <div class="row" style=" margin: 10px 0px 0px 0px; ">
        <div class="col-md-12">  
        <span style='font-size:10.0pt;font-family:"Palatino Linotype",serif;font-weight: normal;'> W* Watcher present: (&#10004;)	 
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         MOB* Mobility (M) : Ambulatory (A), Assisted (S), Bedridden (BR)
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <strong> By: Nursing Department Team Feb. 2021</strong>
        </span>
    </div>
    </div>
</div>
<!-- ./wrapper -->


<script>

</script>

<script>

$(document).ready(function($) {
  
  $('.txtdata').on('change', function(){
    var parent = $(this).parent('.eachcolsub').parent('.eachcol').parent('.eachrow');
    var convertedIntoArray = {};


    
////////////////////////////////////////////////////////////////
var nursingdate={};
var dpfc = document.getElementById("dpfc").value;
nursingdate['dpfc']= dpfc;
var nprm = document.getElementById("nprm").value;
nursingdate['nprm']= nprm;
var dnm = document.getElementById("dnm").value;
nursingdate['dnm']= dnm;
var nnm = document.getElementById("nnm").value;
nursingdate['nnm']= nnm;
var ddf = document.getElementById("ddf").value;
nursingdate['ddf']= ddf;
var ndf = document.getElementById("ndf").value;
nursingdate['ndf']= ndf;
var ocr = document.getElementById("ocr").value;
nursingdate['ocr']= ocr;
var cssd = document.getElementById("cssd").value;
nursingdate['cssd']= cssd;
var dsn = document.getElementById("dsn").value;
nursingdate['dsn']= dsn;
var nsn = document.getElementById("nsn").value;
nursingdate['nsn']= nsn;
var dpnr = document.getElementById("dpnr").value;
nursingdate['dpnr']= dpnr;
var npnr = document.getElementById("npnr").value;
nursingdate['npnr']= npnr;
var ddr = document.getElementById("ddr").value;
nursingdate['ddr']= ddr;
var ndr = document.getElementById("ndr").value;
nursingdate['ndr']= ndr;
var mcr = document.getElementById("mcr").value;
nursingdate['mcr']= mcr;
var dec = document.getElementById("dec").value;
nursingdate['dec']= dec;
var dna = document.getElementById("dna").value;
nursingdate['dna']= dna;
var nna = document.getElementById("nna").value;
nursingdate['nna']= nna;
var dpull = document.getElementById("dpull").value;
nursingdate['dpull']= dpull;
var npull = document.getElementById("npull").value;
nursingdate['npull']= npull;

// console.log(nsn);
/////////////////////


   $("table#details tr").each(function() {
      var rowDataArray = [];
      var night = $(this).find('td').find('.night').find('input').val();
      var day = $(this).find('td').find('.day').find('input').val();
      var m = $(this).find('td').find('.m').find('input').val();

      var w = $(this).find('td').find('.w').find('input[name$="w"]');
    if(w.prop('checked') === true){
          var w1 = 'yes';
        }else{
          var w1 = '';
        }





      var actualData = $(this).find('td');
      if (actualData.length > 0) {
         actualData.each(function() {
            rowDataArray.push($(this).text());
            if (rowDataArray[0] > 0) {
                var DataArray={};
                DataArray['id']=rowDataArray[0];
                DataArray['bed']=rowDataArray[1];
                DataArray['mrn']=rowDataArray[2];
                DataArray['name']=rowDataArray[3];
                DataArray['age']=rowDataArray[4];
                DataArray['w']=w1;
                DataArray['m']=m;
                DataArray['day']=day;
                DataArray['night']=night;
                convertedIntoArray[rowDataArray[0]]=DataArray;
            }
         });
      }
   });
//    console.log(convertedIntoArray);
   let date= new Date('<?php echo  $date1; ?>');
   var currentdate = date.toLocaleDateString();
      console.log();

    data = {currentdate:currentdate, convertedIntoArray: convertedIntoArray, nursingdate:nursingdate};
    $.post('picu-nurse-update.php', data, function(data){
    //   $(parent).html(data);
 
    });
    $(this).parent('.eachcolsub').css("backgroundColor", "#90EE90");
    $(this).parent('.eachcolsub').css("transition", "all 1s");
    
        // $(this).parent('.eachcolsub').css("backgroundColor", "#900090");  
  
  
  });
//   $(this).parent('.eachcolsub').css("transition", "all 2s");
    
});
</script>
</body>
</page>
</html>
