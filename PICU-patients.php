<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
date_default_timezone_set('Asia/Riyadh');
$today=date("Y-m-d");

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');

  if (isset($_POST['add_procedure'])) {
    // receive all input values from the form
    $procedures = mysqli_real_escape_string($mysqli, $_POST['procedures']);
    $proceduredate = mysqli_real_escape_string($mysqli, $_POST['proceduredate']);
    $member_id2=mysqli_real_escape_string($mysqli, $_POST['member_id']);

        $query = "INSERT INTO patients_procedures (procedure_id, member_id,procedure_date ) VALUES ('".$procedures."','".$member_id2."','".$proceduredate."') ";
        // mysqli_query($mysqli, $query);

        if (!$mysqli -> query( $query)) {
          echo("Error description: " . $mysqli -> error);
        }
        // header('location: PICU-patients.php');
    
  }

  if (isset($_POST['add_complication'])) {
    // receive all input values from the form
    $complication = mysqli_real_escape_string($mysqli, $_POST['complications']);
    $complicationdate = mysqli_real_escape_string($mysqli, $_POST['complicationdate']);
    $member_id3=mysqli_real_escape_string($mysqli, $_POST['member_id']);
    if (isset($_POST['associated_procedure_id'])) {
    $associated_procedure=mysqli_real_escape_string($mysqli, $_POST['associated_procedure_id']);
    }else{
      $associated_procedure='';
    }
        $query = "INSERT INTO patients_complications (complication_id, member_id, complication_date, associated_procedure_id ) VALUES
         ('".$complication."','".$member_id3."','".$complicationdate."','".$associated_procedure."') ";
        // mysqli_query($mysqli, $query);

        if (!$mysqli -> query( $query)) {
          echo("Error description: " . $mysqli -> error);
        }
        // header('location: PICU-patients.php');
    
  }


  if (!in_array($user['position'],$access_PICU_patients)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }
?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>


function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}

function admission() {

// var rowname= "row";
// rowname+=value;
// row = document.getElementById(rowname);
//   var id = value;
bed_new=document.getElementById('bed_new').value;
mrn_new=document.getElementById('mrn_new').value;
pname_new=document.getElementById('pname_new').value;
gender_new=document.getElementById('gender_new').value;
nationality_new=document.getElementById('nationality_new').value;
birthdate_new=document.getElementById('birthdate_new').value;
admdate_new=document.getElementById('admdate_new').value;
wt_new=document.getElementById('wt_new').value;
ht_new=document.getElementById('ht_new').value;
admfrom_new=document.getElementById('admfrom_new').value;
var checked = document.querySelectorAll('#admissiondiagnosis_new :checked');
var admissiondiagnosis_new = [...checked].map(option => option.value);
var checked1 = document.querySelectorAll('#comorbidities_new :checked');
var comorbidities_new = [...checked1].map(option => option.value);
var parent = document.getElementById('messsssage');
// alert(patientId);
if(bed_new==""){
            //do something
            // alert("name can not be null");
            return false;
            //this will not submit your form
        }
        else if(mrn_new==""){
            return false;
        }
        else if(pname_new==""){
          // alert(mrn_new);
            return false;
        }
        else if(gender_new==""){
            return false;
        }
        else if(nationality_new==""){
            return false;
        }
        else if(birthdate_new==""){
            return false;
        }
        else if(admdate_new==""){
            return false;
        }
        else if(ht_new==""){
            return false;
        }
        else if(wt_new==""){
            return false;
        }
        else if(admfrom_new==""){
            return false;
        }
        else if(admissiondiagnosis_new==""){
            return false;
        }
        else{
          data = {bed_new: bed_new, mrn_new: mrn_new, gender_new: gender_new, pname_new: pname_new, nationality_new: nationality_new,
            birthdate_new:birthdate_new,ht_new:ht_new,wt_new:wt_new, admdate_new:admdate_new,admfrom_new:admfrom_new, comorbidities_new:comorbidities_new, admissiondiagnosis_new:admissiondiagnosis_new};
            // alert(mrn_new);
            $.post('PICU-patients-add.php', data, function(data){
// $(parent).html(data);
location.reload();
});
// alert(data);
//This will submit your form.
        }
// alert(patientId);
//   row.style.display = "none";
  }

// function discharge() {

// // var rowname= "row";
// // rowname+=value;
// // row = document.getElementById(rowname);
// //   var id = value;
// patientId=document.getElementById('patientId').value;
// disdate=document.getElementById('disdate').value;
// finaldiagnosis=document.getElementById('finaldiagnosis').value;
// disstatus=document.getElementById('disstatus').value;
// disto=document.getElementById('disto').value;
// var parent = document.getElementById('messsssage');
// // alert(patientId);
// if(disto==""){
//             //do something
//             // alert("name can not be null");
//             return false;
//             //this will not submit your form
//         }
//         else if(finaldiagnosis==""){
//             //do something
//             // alert("phone can not be null");
//             return false;
//             //this will not submit your form
//         }
//         else if(disstatus==""){
//             //do something
//             // alert("phone can not be null");
//             return false;
//             //this will not submit your form
//         }
//         else if(disto==""){
//             //do something
//             // alert("phone can not be null");
//             return false;
//             //this will not submit your form
//         }
//         else{
//           data = {patientId: patientId, disdate: disdate, finaldiagnosis: finaldiagnosis, disstatus: disstatus, disto: disto};
//   $.post('PICU-patients-discharge.php', data, function(data){
// $(parent).html(data);
// // location.reload();
// });
// //This will submit your form.
//         }
// // alert(patientId);
// //   row.style.display = "none";
//   }

function del_procedure(value){
  if(!confirm("Do you really want to delete this procedure")) {
    return false;
  }
  var rowname= "pro_row";
  rowname+=value;
  li = document.getElementById(rowname);
    var id = value;
    data = {id: id};
    $.post('PICU-patients-pprocedure-delete.php', data, function(data){
    // $(parent).html(data);
  });
    li.style.display = "none";
    }

    function del_complication(value){
  if(!confirm("Do you really want to delete this procedure")) {
    return false;
  }
  var rowname= "comp_row";
  rowname+=value;
  li = document.getElementById(rowname);
    var id = value;
    data = {id: id};
    $.post('PICU-patients-pcomplication-delete.php', data, function(data){
    // $(parent).html(data);
  });
    li.style.display = "none";
    }
 
    function sortTable(n) {
  var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < rows.length - 1; i++) { //Change i=0 if you have the header th a separate table.
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php
    $formationSQL = "SELECT * FROM procedures";
    $result1 = $mysqli->query($formationSQL);
    $procedures = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM complications";
		$result1 = $mysqli->query($formationSQL);
		$complications = $result1 -> fetch_all(MYSQLI_ASSOC);
		
		$formationSQL = "SELECT * FROM picupatients WHERE DISDATE IS NULL";
		$result1 = $mysqli->query($formationSQL);
		$activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM countries";
		$result1 = $mysqli->query($formationSQL);
		$countries = $result1 -> fetch_all(MYSQLI_ASSOC);

	?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Active PICU Patients</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Active PICU Patients</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Active PICU Patients</h3>
                <div id="addbtn" class='eachrow' style=' float: right; '>
  
                <a  class='btn btn-success'  href='#admiting_modal' data-toggle='modal'  style='color: aliceblue; line-height: 2;padding: 0px 15px;'>Add New Patient</a>
  
                <a  type='button' class="btn btn-success" target='_blank' href="picu-nurse-daily.php?date=<?php echo $today; ?>"  style="color: aliceblue; line-height: 2;padding: 0px 15px;"  id= 'NADS'>Nursing Assignment</a></td>
        
          </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">

                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                              <div class="row">                 
                       
                                         <?php

                                                     foreach($activepicupatints as $s){
                                                    
                                                  
                                                      $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
                                                      $decodedcomorbidities=json_decode($s['comorbidities']);
                                                      
                                                      $birthdate=$s['birthdate'];
                                                      $today = date("Y-m-d");
                                                      $diff = date_diff(date_create($birthdate), date_create($today));
                                                      $age =$diff->format('%y');
                                                      if ($age < 1){
                                                          $age = "<1 Year";
                                                      }

                                                    echo"  
                                                    <div class='col-sm-3'>
                                                    <div class='eachrow card'  id='row".$s['ID']."'>
                                                    
                                                      <div style='  margin: 2%; display: inline; ' class='eachcol bed card-header'  scope='row' ><input class='txtdata' name='bed' placeholder='Bed Number' value='".$s['BED']."' style='text-align: center;width: 85%;' >
                                                      <input class='txtdata' type='hidden' name='id' id='id' value='".$s['ID']."' style='text-align: center;width: 85%;' >
                                                      <a  href='emergency-medications.php?age=".$age."&wt=".$s['wt']."&ht=".$s['ht']."' target='_blank'><i style='font-size: 20px;color: brown;vertical-align: middle;' class='fas fa-capsules'></i></a>
                                                      </div>
                                                      
                                                      <div style=' margin: 2%; ' class='eachcol mrn' >
                                                      <label style='margin: 0px; text-align: center; '>MRN</label>
                                                      <input class='txtdata' name='mrn' value='".$s['MRN']."' style='text-align: center;'>
                                                      </div>

                                                      <div style=' margin: 2%; ' class='eachcol name'>
                                                      <label style='margin: 0px; text-align: center; '>Patient Name</label>
                                                      <input class='txtdata' name='name' value='".$s['PNAME']."' style='text-align: center;'>
                                                      </div>
                                                
                                                      <div style=' margin: 2%; ' class='eachcol admdate'  scope='row' >
                                                      <label style='margin: 0px; text-align: center; '>Admission Date</label>
                                                      <input type='text' onfocus='(this.type='date')' class='txtdata' name='admdate'  id='datepicker' value='".$s['ADMDATE']."' style='text-align: center;padding: 0px;'>
                                                      </div>
                                                      ";
                                                      // echo"
                                                    //   <div style=' margin: 2%; ' class='eachcol admfrom'  scope='row' >
                                                    //   <select class='txtdata' name='admfrom' style='text-align: center;' >
                                                    //   ";
                                                    //             if (!empty($s['ADMFROM'])){
                                                    //               echo"<option selected  value='".$s['ADMFROM']."'>".$s['ADMFROM']."</option>";
                                                    //             }else{
                                                    //               echo " <option selected disabled value=''>Select</option>";
                                                    //             }
                                                    //   echo"
                                                     
                                                    //   <option value='Ward'>Ward</option>
                                                    //   <option value='Emergency'>Emergency</option>
                                                    //   <option value='OR'>OR</option>
                                                    //   <option value='Referral'>Referral</option>
                                                    // </select>
                                                    //   </div>";
                                                
                                                 echo"
                                                      <div style=' margin: 2%; ' class='eachcol comorbidities'>
                                                      <label style='margin: 0px; text-align: center; '>Comorbidities</label>
                                                      <select class='txtdata ddxname form-control' style='width: 100%;' oninput='auto_grow(this)'  multiple='multiple' name='comorbidities'>
                                                      ";
                                                
                                                      if (is_array($decodedcomorbidities)){
                                                        
                                                        foreach($decodedcomorbidities as $key => $value)
                                                  {
                                                
                                                    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
                                                    $result1 = $mysqli->query($formationSQL);
                                                    $dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                     
                                                
                                                      echo '<option selected value="' . $dxlist['id'] . '">'.  $dxlist['name']. '</option>';
                                                      
                                                  }}
                                                
                                                      echo"
                                                      </select></div>
                                                      <div style=' margin: 2%; ' class='eachcol admissiondiagnosis'>
                                                      <label style='margin: 0px; text-align: center; '>Initial Diagnosis</label>
                                                      <select class='txtdata ddxname form-control' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' name='admissiondiagnosis'>
                                                      ";
                                                
                                                      if (is_array($decodedadmissiondx)){
                                                        
                                                        foreach($decodedadmissiondx as $key => $value)
                                                  {
                                                    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
                                                    $result1 = $mysqli->query($formationSQL);
                                                    $dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                     
                                                
                                                      echo '<option selected value="' . $dxlist['id'] . '">'.  $dxlist['name']. '</option>';
                                                  }}
                                                
                                                      echo"
                                                      </select></div>
                                                
                                                     
                                                      <div style='  margin:2%; ' class='eachcol procedures'>
                                                      <label style='margin: 0px; text-align: center;'>Procedures <a href='#procedures_modal' data-book-id='".$s['ID']."' data-toggle='modal' ><i style='font-size: 20px;color: forestgreen;vertical-align: sub;' class='fas fa-plus'></i></a></label>
                                                      <ul class='fa-ul' style='margin:0px'>
                                                     ";


                                                     $formationSQL = "SELECT * FROM patients_procedures WHERE member_id='".$s['ID']."'";
                                                     $result1 = $mysqli->query($formationSQL);
                                                     $procedure_list = $result1 -> fetch_all(MYSQLI_ASSOC);
                                                     
                                                     foreach($procedure_list as $pl){

                                                      $formationSQL = "SELECT * FROM procedures WHERE id='".$pl['procedure_id']."'";
                                                      $result1 = $mysqli->query($formationSQL);
                                                      $procedure_name = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                      // var_dump($procedure_name);
                                                       echo "<li id='pro_row".$pl['ID']."'><a type='button' class='btn-tool'  style=''   onclick='del_procedure(".$pl['ID'].")' class='fa-li'><i class='fa fa-trash'></i></a>".  $procedure_name['procedurename']. " on ".  $pl['procedure_date']. " </li>";
                                                      }

                                                      echo" </ul>
                                                      </div>
                                                
                                                
                                                      <div style=';margin: 2%;' class='eachcol complications '>
                                                      
                                                      <label style='margin: 0px; text-align: center; '>Complications <a href='#complication_modal' data-book-id='".$s['ID']."' data-toggle='modal' ><i style='font-size: 20px;color: forestgreen;vertical-align: sub;' class='fas fa-plus'></i></a></label>
                                                    
                                                      <ul class='fa-ul' style='margin:0px'>
                                                     ";
                                                     $formationSQL = "SELECT * FROM patients_complications WHERE member_id='".$s['ID']."'";
                                                     $result1 = $mysqli->query($formationSQL);
                                                     $complication_list = $result1 -> fetch_all(MYSQLI_ASSOC);
                                                     
                                                     foreach($complication_list as $pl){

                                                      $formationSQL = "SELECT * FROM complications WHERE id='".$pl['complication_id']."'";
                                                      $result1 = $mysqli->query($formationSQL);
                                                      $complication_name = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                      // var_dump($procedure_name);
                                                       echo '<li id="comp_row'.$pl['ID'].'"><a type="button" class="btn-tool"  onclick="del_complication('.$pl['ID'].')" class="fa-li"><i class="fa fa-trash"></i></a>'.  $complication_name['complication']. ' on '.  $pl['complication_date']. ' ';
                                                      
                                                        if ($complication_name['require_associated_procedure']=="1"){
                                                          $formationSQL = "SELECT * FROM procedures WHERE id='".$pl['associated_procedure_id']."'";
                                                          $result1 = $mysqli->query($formationSQL);
                                                          $procedure_name = $result1 -> fetch_array(MYSQLI_ASSOC);
                                                          $formationSQL = "SELECT * FROM patients_procedures WHERE procedure_id='".$pl['associated_procedure_id']."'";
                                                          $result1 = $mysqli->query($formationSQL);
                                                          $procedure_list = $result1 -> fetch_array(MYSQLI_ASSOC);

                                                            echo " related to ". $procedure_name['procedurename']." on ". $procedure_list['procedure_date'] ;
                                                        }
                                                        echo "</li>"; 
                                                        }
                                                      echo" 
                                                      </ul>
                                                      
                                                      </div>
                                                
                                                
                                                   <div style=' margin: 2%; ' class='eachcol dnr'>";
                                                   if (!empty($s['DNR'])){
                                                     echo"<input style='width: auto;' class='txtdata' type='checkbox' name='dnr' value='DNR' checked>
                                                     ";
                                                   }else{
                                                     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='dnr' value='DNR'>";
                                                   }
                                                   echo "<label for='dnr' style=' display: contents; '> DNR</label><br>";
                                                   
                                                   if (!empty($s['braindeath'])){
                                                     echo"<input style='width: auto;' class='txtdata'  type='checkbox' name='braindeath' value='braindead' checked>
                                                     ";
                                                   }else{
                                                     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='braindeath' value='braindead'>";
                                                   }
                                                   echo "<label for='braindeath' style=' display: contents; '> Brain Dead</label><br>";
                                                   
                                                   if (!empty($s['scot'])){
                                                     echo"<input style='width: auto;' class='txtdata'  type='checkbox' name='scot' value='SCOT' checked>
                                                     ";
                                                   }else{
                                                     echo " <input style='width: auto;' class='txtdata'  type='checkbox' name='scot' value='SCOT'>";
                                                   }
                                                   echo "<label for='scot' style=' display: contents; '> SCOT Involved </label>";
                                                   
                                                   
                                                   echo"
                                                   </div>
                                                
                                                   <div style=' margin: 2%; ' class='eachcol id' scope='row' ><input class='txtdata' name='id' value='".$s['ID']."' style='display: none;'>
                                                   
                                                   <a class='btn btn-info' href='#details_modal' data-book-id='".$s['ID']."' data-toggle='modal'  style='color: aliceblue;line-height: 2;padding: 0px 10%;margin: 2%;'>Details</a>
                                                   
                                                   <a  class='btn btn-success'  href='#my_modal' data-toggle='modal'  data-book-id='".$s['ID']."'  style='color: aliceblue;line-height: 2;padding: 0px 10%;margin: 2%;'  id= 'discharge'>Discharge</a>
                                                   
                                                      </div>
                                                         </div >
                                                         </div >
                                                      ";

                                                     }
                                         ?> 
                        

</div>




<!-- Modal for admitting patient -->

<div class="modal" id="admiting_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Admit New Patient</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Kindly Complete the Required Information</p>


        <form autocomplete="off"  method="POST">
        <label>Bed Number</label>
        <input class='txtdata' id='bed_new' value='' style='text-align: center;' required>
        <label>MRNs</label>
        <input class='txtdata' id='mrn_new' value='' style='text-align: center;' required>
        <label>Patient Name</label>
        <input class='txtdata' id='pname_new' value='' style='text-align: center;' required>
        <label>Gender</label>
        <select class='txtdata' id='gender_new' style='text-align: center;' required>
      <option value='Male'>Male</option>
      <option value='Female'>Female</option>
      <option value='Unknown'>Unknown</option>
    </select>
    <label>Height (Cm)</label>
        <input class='txtdata' id='ht_new' value='' style='text-align: center;' required>
        <label>Weight (Kg)</label>
        <input class='txtdata' id='wt_new' value='' style='text-align: center;' required>
        <label>Nationality</label>
        <select class='select2 txtdata' id='nationality_new' style='text-align: center;' required>
         <?php   
        foreach($countries as $country)
            echo"
            <option value='".$country['name']."'>".$country['name']."</option>";
          ?>
    
  </select>
        <label>Birth date</label>
        <input type='text' onfocus='(this.type="date")' class='txtdata' id='birthdate_new' name='birthdate'  style='text-align: center;padding: 0px;'required>

        <label>Admittion date</label>
        <input value='' class='txtdata' id ="admdate_new"  data-date-format="DD-MM-YYYY HH:mm:ss" type="text"  name='admdate' style="text-align: center;padding: 0px;" required>
        

        <label>Admitted from</label>
        <select class='txtdata' id ="admfrom_new" style="width: 100%;text-align: center;padding: 4px;" required>
        <option selected disabled value=''>Select</option>"
        <option value='Ward'>Ward</option>
      <option value='Emergency'>Emergency</option>
      <option value='OR'>OR</option>
      <option value='Referral'>Referral</option>
    </select>
    
    <label>Comorbidities</label>
    <select class='txtdata ddxname form-control' style='width: 100%;' oninput='auto_grow(this)'  multiple='multiple' id='comorbidities_new'></select>
    <label>Admission Diagnosis</label>
    <select class='txtdata ddxname form-control' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' id='admissiondiagnosis_new' required></select>
      </div>

      <div class="modal-footer">
<?php
     echo" <button type='submit' value='submit' class='btn btn-success'  onclick='admission()'>Complete Admission</button>";
  ?>
      <button type="button" class="btn btn-default" style=" color: black; " data-dismiss="modal">Close</button>
    <div id='messsssage'></div>
      </div>

          </form>
    </div>
  </div>
</div>


<!-- procedures_modal-->

<div class="modal" id="procedures_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Add a Procedure</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="pproceduressdiv"></div>

     
    </div>
  </div>
</div>
</div>    


<!-- complication_modal-->

<div class="modal" id="complication_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Add a Complication</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="pcomplicationsdiv"></div>

     
    </div>
  </div>
</div>
</div>    


<!-- Modal patient details-->

<div class="modal" id="details_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Patient Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    <!-- data retrived from JS -->
      <div class="modal-body">
      <div id="pdetailsdiv"></div>
        
     
    </div>
  </div>
</div>
</div>    

<!-- Modal -->

<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
     
          <h4 class="modal-title">Discharge Patient</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <p>Kindly Review admission and discharge details</p>

        <div id="pdischargediv"></div>
       
      </div>

      <div class="modal-footer">
<?php
    //  echo" <button type='submit' value='submit' class='btn btn-danger'  onclick='discharge()'>Complete Discharge</button>";
  ?>
      <!-- <button type="button" class="btn btn-default" style=" color: black; " data-dismiss="modal">Close</button> -->
    <!-- <div id='messsssage'></div> -->
      </div>

          </form>
    </div>
  </div>
</div>
           
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
 </div> <!--row -->
			
 

<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>


<?php
	



?>
</div><!--/. container-fluid -->

    </section>
    <!-- /.content -->
    


  </div>
  <!-- /.content-wrapper -->


  <script type="text/javascript">
      $(document).ready(function() {
        $('.select2').select2({
      placeholder: 'Select',
      
    } );
        $('.ddxname').select2({
            placeholder: 'Select',
            minimumInputLength: 4,
            ajax: {
                url: 'fetchicd10.php',
                dataType: 'json',
                delay: 250,
                data: function (data) {
                    return {
                        searchTerm: data.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results:response
                    };
                },
                cache: true
            }
        });
      });

    </script>
  <script>





$(document).ready(function($) {
  
  $('.txtdata').on('change', function(){
var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    // var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var comorbidities = $(parent).find('.comorbidities').find('select').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
   

    var dnrbox = $(parent).find('.dnr').find('input[name$="dnr"]');
    if(dnrbox.prop('checked') === true){
          var dnr = $(parent).find('.dnr').find('input[name$="dnr"]').val();
        }else{
          var dnr = '';
        }

    var braindeathbox  = $(parent).find('.dnr').find('input[name$="braindeath"]');
    if(braindeathbox.prop('checked') === true){
          var braindeath = $(parent).find('.dnr').find('input[name$="braindeath"]').val();
        }else{
          var braindeath = '';
        }
      

var scotbox  = $(parent).find('.dnr').find('input[name$="scot"]');
    if(scotbox.prop('checked') === true){
          var scot = $(parent).find('.dnr').find('input[name$="scot"]').val();
        }else{
          var scot = '';
        }
      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {id: id, bed: bed, mrn: mrn,name: name,  admdate: admdate
      // , admfrom: admfrom
      , admissiondiagnosis:admissiondiagnosis, comorbidities: comorbidities,  dnr:dnr, braindeath:braindeath, scot:scot, attribChanged: attribChanged};
    $.post('PICU-patients-update.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});
</script>

<script>

// document.getElementById('addpatient').onclick = function(){
//   var parent = $(this).parent('.eachrow');
//   var attribChanged = $(this).attr('name');
//   data = {attribChanged: attribChanged};
//   $.post('PICU-patients-add.php', data, function(data){
//   $(parent).html(data);
//   location.reload();
// });
// }



$("textarea").each(function(textarea) {
    $(this).height( $(this)[0].scrollHeight );
});


  </script>


<script>
$(function() {
  $('input[name="admdate"]').daterangepicker({
    singleDatePicker: true,
    timePicker: true,
    timePicker24Hour: true,
    autoUpdateInput: false,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD  h:mm A'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
        var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    // var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var comorbidities = $(parent).find('.comorbidities').find('select').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
   
 

    var dnrbox = $(parent).find('.dnr').find('input[name$="dnr"]');
    if(dnrbox.prop('checked') === true){
          var dnr = $(parent).find('.dnr').find('input[name$="dnr"]').val();
        }else{
          var dnr = '';
        }

    var braindeathbox  = $(parent).find('.dnr').find('input[name$="braindeath"]');
    if(braindeathbox.prop('checked') === true){
          var braindeath = $(parent).find('.dnr').find('input[name$="braindeath"]').val();
        }else{
          var braindeath = '';
        }
      

var scotbox  = $(parent).find('.dnr').find('input[name$="scot"]');
    if(scotbox.prop('checked') === true){
          var scot = $(parent).find('.dnr').find('input[name$="scot"]').val();
        }else{
          var scot = '';
        }
      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {id: id, bed: bed, mrn: mrn,name: name,  admdate: admdate,
      // admfrom: admfrom,
       admissiondiagnosis:admissiondiagnosis, comorbidities: comorbidities,   dnr:dnr, braindeath:braindeath, scot:scot, attribChanged: attribChanged};
    $.post('PICU-patients-update.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");
    });
});


</script>

<script>




</script>
<?php

require 'footer.php';

?>

<script>




$('#my_modal').on('show.bs.modal', function(e) {
 
  var bookId = $(e.relatedTarget).data('book-id');
  

 data = {bookId: bookId};
 $.post('PICU-patients-discharge.php', data, function(data){
 $('#pdischargediv').html(data);
     
    });
  
    // $(e.currentTarget).find('input[name="patientId"]').val(bookId);
    // disdate=document.getElementById('disdate').value='';
    // document.getElementById('finaldiagnosis').value='';
    // document.getElementById('disstatus').value='';
    // document.getElementById('disto').value='';
    // document.getElementById('disdate').style.backgroundColor = "";
    // document.getElementById('finaldiagnosis').style.backgroundColor = "";
    // document.getElementById('disto').style.backgroundColor = "";
    // document.getElementById('disstatus').style.backgroundColor = "";
});

$('#details_modal').on('show.bs.modal', function(e) {
 
//  var bookId = $(e.relatedTarget).data('book-id');
 var bookId = $(e.relatedTarget).data('book-id');
//  $(e.currentTarget).find('input[name="patientId"]').val(bookId);


 data = {bookId: bookId};
 $.post('PICU-patients-details.php', data, function(data){
 $('#pdetailsdiv').html(data);
     
    });
});

$('#procedures_modal').on('show.bs.modal', function(e) {
 
 //  var bookId = $(e.relatedTarget).data('book-id');
  var myBookId = $(e.relatedTarget).data('book-id');
 //  $(e.currentTarget).find('input[name="patientId"]').val(bookId);
//  $(".modal-body #bookId").val( myBookId );
 

  data = {myBookId: myBookId};
  $.post('PICU-patients-pproceduressdiv.php', data, function(data){
  $('#pproceduressdiv').html(data);

      
     });
 });

 $('#complication_modal').on('show.bs.modal', function(e) {
 
 //  var bookId = $(e.relatedTarget).data('book-id');
  var myBookId1 = $(e.relatedTarget).data('book-id');
 //  $(e.currentTarget).find('input[name="patientId"]').val(bookId);
//  $(".modal-body #bookId").val( myBookId );
 

  data = {myBookId1: myBookId1};
  $.post('PICU-patients-pcomplicationsdiv.php', data, function(data){
  $('#pcomplicationsdiv').html(data);

      
     });
 });

$(function() {
  $('input[name="birthdate"]').daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: false,
    showDropdowns: true,
    autoApply: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
      
  });
});
</script>

