<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}

?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');
  if (!in_array($user['position'],$access_PICU_registry)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }
?>

     <style>
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>


function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}


function del(value){
  if(!confirm("Do you really want to delete this user?")) {
    return false;
  }
  var rowname= "row";
  rowname+=value;
  row = document.getElementById(rowname);
    var id = value;
    data = {id: id};
    $.post('PICU-registry-delete.php', data, function(data){
    // $(parent).html(data);
  });
    row.style.display = "none";
    }
 


 
    function sortTable(n) {
  var table,
    rows,
    switching,
    i,
    x,
    y,
    shouldSwitch,
    dir,
    switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < rows.length - 1; i++) { //Change i=0 if you have the header th a separate table.
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php

    $formationSQL = "SELECT * FROM procedures";
    $result1 = $mysqli->query($formationSQL);
    $procedures = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM complications";
		$result1 = $mysqli->query($formationSQL);
		$complications = $result1 -> fetch_all(MYSQLI_ASSOC);


		$formationSQL = "SELECT * FROM picupatients";
		$result1 = $mysqli->query($formationSQL);
		$activepicupatints = $result1 -> fetch_all(MYSQLI_ASSOC);

    $formationSQL = "SELECT * FROM countries";
		$result1 = $mysqli->query($formationSQL);
		$countries = $result1 -> fetch_all(MYSQLI_ASSOC);


	?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Active PICU Patients</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Active PICU Patients</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> Active PICU Patients</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
<?php 

   ?>
                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                        <div  class="table-responsive"> 
                        <table id="myTable" class="table table-sm " cellspacing="0" width="100%">
                <thead>
                  <tr>
                
                    <th onclick="sortTable(0)" class="th-sm" scope="col" >action <i class="fa fa-sort" aria-hidden="true"></i></th>

                    <th class="th-sm" scope="col" >MRN</th>
                    <th class="th-sm" scope="col" >Patient Name</th>
                    <th class="th-sm" scope="col" >Gender</th>
                    <th class="th-sm" scope="col" >Height</th>
                    <th class="th-sm" scope="col" >Weight</th>
                    <th class="th-sm" scope="col" >Nationality</th>
                    <th class="th-sm" scope="col" >Birth date</th>
                    <th class="th-sm" scope="col" >Admitted From</th>
                    <th class="th-sm" scope="col" >Admission Date</th>
                    <th class="th-sm" scope="col" >Discharge Date</th>
                    <th class="th-sm" scope="col" >Discharge To</th>
                    <th class="th-sm" scope="col" >Status</th>

                    <th class="th-sm" scope="col" >Comorbidities</th>
                    <th class="th-sm" scope="col" >Admission Diagnosis</th>
                    <th class="th-sm" scope="col" >Discharge Diagnosis</th>
                    <th class="th-sm" scope="col" >Procedures</th>
                    <th class="th-sm" scope="col" >Complications</th>
                    <th class="th-sm" scope="col" >Others</th>

                  </tr>
                </thead>
                <tbody>
                <form autocomplete="off">

                  <?php

                                                                         
usort($activepicupatints, function($a, $b) {
  return $a['BED'] <=> $b['BED'];
});

// $decodedP=array();                       
            foreach($activepicupatints as $s){
              $decodedP=json_decode($s['PROCEDURES']);
              $decodedC=json_decode($s['COMPLICATIONS']);
              $decodedadmissiondx=json_decode($s['admissiondiagnosis']);
              $decodeddischargedx=json_decode($s['dischargediagnosis']);
              $decodedcomorbidities=json_decode($s['comorbidities']);
       echo " 
     
       <tr class='eachrow' id='row".$s['ID']."'>
     
      <td class='eachcol id'  scope='row' >  <input class='txtdata' type='hidden' name='id' value='".$s['ID']."' disabled>
      <a  type='button' class='btn-tool'  style=''   onclick='del(".$s['ID'].")'><i class='fa fa-trash'></i></a></td>
      
 

      <td class='eachcol mrn' ><input class='txtdata' name='mrn' value='".$s['MRN']."' ></td>
      <td class='eachcol name'><input class='txtdata' name='name' value='".$s['PNAME']."' ></td>
      <td class='eachcol gender'>
      <select class='txtdata' name='gender'  >
      ";
                if (!empty($s['gender'])){
                  echo"<option selected  value='".$s['gender']."'>".$s['gender']."</option>";
                }else{
                  echo " <option selected disabled value=''>Select</option>";
                }
      echo"
      <option value='Male'>Male</option>
      <option value='Female'>Female</option>
      <option value='Unknown'>Unknown</option>
    </select>
    </td>
    <td class='eachcol ht'><input class='txtdata' name='ht' value='".$s['ht']."' ></td>
    <td class='eachcol wt'><input class='txtdata' name='wt' value='".$s['wt']."' ></td>
    <td class='eachcol nationality'>
    <select class='select2 txtdata' name='nationality'  >
    ";
              if (!empty($s['nationality'])){
                echo"<option selected  value='".$s['nationality']."'>".$s['nationality']."</option>";
              }else{
                echo " <option selected disabled value='SA'>Saudi Arabia</option>";
              }


foreach($countries as $country)
    echo"
    <option value='".$country['name']."'>".$country['name']."</option>";

    echo"
  </select>
  </td>
      <td class='eachcol birthdate'  scope='row' ><input type='text' onfocus='(this.type='date')' class='txtdata' name='birthdate'  id='datepicker' value='".$s['birthdate']."' ></td>
      <td class='eachcol admfrom'  scope='row' >
      <select class='txtdata' name='admfrom'  >
      ";
                if (!empty($s['ADMFROM'])){
                  echo"<option selected  value='".$s['ADMFROM']."'>".$s['ADMFROM']."</option>";
                }else{
                  echo " <option selected disabled value=''>Select</option>";
                }
      echo"
      <option value='Ward'>Ward</option>
      <option value='Emergency'>Emergency</option>
      <option value='OR'>OR</option>
      <option value='Referral'>Referral</option>
    </select>
      </td>
      <td class='eachcol admdate'  scope='row' ><input type='text' onfocus='(this.type='date')' class='txtdata' name='admdate'  id='datepicker' value='".$s['ADMDATE']."' ></td>
      <td class='eachcol disdate'  scope='row' ><input type='text' onfocus='(this.type='date')' class='txtdata' name='disdate'  id='datepicker' value='".$s['DISDATE']."' ></td>
      
      <td class='eachcol disto'  scope='row' >
      <select class='txtdata' name='disto'  >
      ";
                if (!empty($s['DISTO'])){
                  echo"<option selected  value='".$s['DISTO']."'>".$s['DISTO']."</option>";
                }else{
                  echo " <option selected disabled value=''>Select</option>";
                }
      echo"
      <option value='Ward'>Ward</option>
      <option value='Home'>Home</option>
      <option value='Mortuary'>Mortuary</option>
      <option value='referred'>Other Facility</option>
    </select>
      </td>
      
      <td class='eachcol status'  scope='row' >
      <select class='txtdata' name='status'  >
      ";
                if (!empty($s['MORTALITY'])){
                  echo"<option selected  value='".$s['MORTALITY']."'>".$s['MORTALITY']."</option>";
                }else{
                  echo " <option selected disabled value=''>Select</option>";
                }
      echo"
      <option value='Alive'>Alive</option>
      <option value='Dead'>Dead</option>
    </select>
      </td>
     
      <td class='eachcol comorbidities'> <select class='txtdata ddxname form-control' style='width: 100%;' oninput='auto_grow(this)'  multiple='multiple' name='comorbidities'>
      ";

      if (is_array($decodedcomorbidities)){
        
        foreach($decodedcomorbidities as $key => $value)
  {

    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
		$result1 = $mysqli->query($formationSQL);
		$dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
      // $selected = in_array($key, $decodedP) ? 'selected ' : '';

      echo '<option selected value="' . $dxlist['id'] . '">'.  $dxlist['name']. '</option>';
      
  }}

      echo"
      </select></td>
      <td class='eachcol admissiondiagnosis'><select class='txtdata ddxname form-control' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' name='admissiondiagnosis'>
      ";

      if (is_array($decodedadmissiondx)){
        
        foreach($decodedadmissiondx as $key => $value)
  {
    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
		$result1 = $mysqli->query($formationSQL);
		$dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
      // $selected = in_array($key, $decodedP) ? 'selected ' : '';

      echo '<option selected value="' . $dxlist['id'] . '">'.  $dxlist['name']. '</option>';
  }}

      echo"
      </select></td>
<td class='eachcol dischargediagnosis'><select class='txtdata ddxname form-control' style='width: 100%;'  oninput='auto_grow(this)'  multiple='multiple' name='dischargediagnosis'>
      ";

      if (is_array($decodeddischargedx)){
        
        foreach($decodeddischargedx as $key => $value)
  {
    $formationSQL = "SELECT * FROM icd10 WHERE id='".$value."'";
		$result1 = $mysqli->query($formationSQL);
		$dxlist = $result1 -> fetch_array(MYSQLI_ASSOC);
      // $selected = in_array($key, $decodedP) ? 'selected ' : '';

      echo '<option selected value="' . $dxlist['id'] . '">'.  $dxlist['name']. '</option>';
  }}

      echo"
      </select></td>

      <td class='eachcol procedures'>
      <select class='txtdata select2' multiple='multiple'style='width: 100%; outline: auto; padding-left: 5px;padding-right: 5px;'; oninput='auto_grow(this)' name='procedures' id='procedures'  >
      ";

      
      if (is_array($decodedP)){
        
      foreach($decodedP as $key => $value)
{
  $formationSQL = "SELECT * FROM procedures WHERE id='".$value."'";
  $result1 = $mysqli->query($formationSQL);
  $procedurelist = $result1 -> fetch_array(MYSQLI_ASSOC);
    // $selected = in_array($key, $decodedP) ? 'selected ' : '';

    echo '<option selected value="' . $procedurelist['id'] . '">'.  $procedurelist['procedurename']. '</option>';
}}

  foreach($procedures as $procedurelist){
    echo"<option value='".$procedurelist['id']."'>".$procedurelist['procedurename']."</option>";
  }
    echo"  
    </select>
      </td>


      <td class='eachcol complications'>
      <select class='txtdata select2' multiple='multiple'style='width: 100%; outline: auto; padding-left: 5px;padding-right: 5px;'; oninput='auto_grow(this)' id='complications' name='complications' value='' >
      ";
      if (is_array($decodedC)){
        
      foreach($decodedC as $key => $value)
{
  $formationSQL = "SELECT * FROM complications WHERE id='".$value."'";
  $result1 = $mysqli->query($formationSQL);
  $complicationlist = $result1 -> fetch_array(MYSQLI_ASSOC);
    // $selected = in_array($key, $decodedP) ? 'selected ' : '';

    echo '<option selected value="' . $complicationlist['id'] . '">'.  $complicationlist['complication']. '</option>';
}}

  foreach($complications as $complicationlist){
    echo"<option value='".$complicationlist['id']."'>".$complicationlist['complication']."</option>";
  }
    echo"  
    </select>
      </td>
<td class='eachcol dnr'>";
if (!empty($s['DNR'])){
  echo"<input class='txtdata' type='checkbox' name='dnr' value='DNR' checked>
  ";
}else{
  echo " <input class='txtdata'  type='checkbox' name='dnr' value='DNR'>";
}
echo "<label for='dnr'> DNR</label><br>";

if (!empty($s['braindeath'])){
  echo"<input class='txtdata'  type='checkbox' name='braindeath' value='braindead' checked>
  ";
}else{
  echo " <input class='txtdata'  type='checkbox' name='braindeath' value='braindead'>";
}
echo "<label for='braindeath'> Brain Dead</label><br>";

if (!empty($s['scot'])){
  echo"<input class='txtdata'  type='checkbox' name='scot' value='SCOT' checked>
  ";
}else{
  echo " <input class='txtdata'  type='checkbox' name='scot' value='SCOT'>";
}
echo "<label for='scot'> SCOT Involved</label>";


echo"
</td>
      </tr >
      ";
      // <textarea style='width: 100%; outline: auto; padding-left: 5px;padding-right: 5px;'; oninput='auto_grow(this)' class='txtdata' name='procedures' value='".$s['PROCEDURES']."' >".$s['PROCEDURES']."</textarea>
      // onclick='discharge(".$s['ID'].")'
    // var_dump($decodedP) ;
    
    }
            // 

            ?>

          </form>
  </tbody>
</table> 
<div id="addbtn" class='eachrow'>
                <td class='eachcol id' scope='row' colspan='6'>
                <a  type='button' class="btn btn-success"  style="color: aliceblue; line-height: 2;padding: 0px 15px;"  id= 'addpatient'> Add New Patient</a></td>
          </div>
</div>
     
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
 </div> <!--row -->
			


<!-- PAGE SCRIPTS -->

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

                       
                    
    
  
</div><!--/. container-fluid -->

    </section>
    <!-- /.content -->


  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
      $(document).ready(function() {
        $('.select2').select2({
      placeholder: 'Select',
    } );
        $('.ddxname').select2({
            placeholder: 'Select',
            minimumInputLength: 4,
            ajax: {
                url: 'fetchicd10.php',
                dataType: 'json',
                delay: 250,
                data: function (data) {
                    return {
                        searchTerm: data.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results:response
                    };
                },
                cache: true
            }
        });
      });
    </script>
  <script>





$(document).ready(function($) {
  
  $('.txtdata').on('change', function(){
    var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    var gender = $(parent).find('.gender').find('select').val();
    var ht = $(parent).find('.ht').find('input').val();
    var wt = $(parent).find('.wt').find('input').val();
    var nationality = $(parent).find('.nationality').find('select').val();
    var birthdate = $(parent).find('.birthdate').find('input').val();
    var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var disdate = $(parent).find('.disdate').find('input').val();
    var disto = $(parent).find('.disto').find('select').val();
    var status = $(parent).find('.status').find('select').val();
    var comorbidities = $(parent).find('.comorbidities').find('select').val();
    var dischargediagnosis = $(parent).find('.dischargediagnosis').find('select').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
    var procedures = $(parent).find('.procedures').find('select').val();
    var complications = $(parent).find('.complications').find('select').val();

    var dnrbox = $(parent).find('.dnr').find('input[name$="dnr"]');
    if(dnrbox.prop('checked') === true){
          var dnr = $(parent).find('.dnr').find('input[name$="dnr"]').val();
        }else{
          var dnr = '';
        }

    var braindeathbox  = $(parent).find('.dnr').find('input[name$="braindeath"]');
    if(braindeathbox.prop('checked') === true){
          var braindeath = $(parent).find('.dnr').find('input[name$="braindeath"]').val();
        }else{
          var braindeath = '';
        }
      

var scotbox  = $(parent).find('.dnr').find('input[name$="scot"]');
    if(scotbox.prop('checked') === true){
          var scot = $(parent).find('.dnr').find('input[name$="scot"]').val();
        }else{
          var scot = '';
        }
      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {gender:gender,ht:ht,wt:wt, nationality:nationality, birthdate:birthdate, dnr:dnr, braindeath:braindeath, scot:scot,  admissiondiagnosis:admissiondiagnosis, dischargediagnosis:dischargediagnosis , status:status, disdate:disdate, disto: disto, id: id, bed: bed,admdate: admdate, admfrom: admfrom, mrn: mrn,name: name,comorbidities: comorbidities,procedures: procedures, complications: complications, attribChanged: attribChanged};
    $.post('PICU-patients-update-registry.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});
</script>

<script>

document.getElementById('addpatient').onclick = function(){
  var parent = $(this).parent('.eachrow');
  var attribChanged = $(this).attr('name');
  data = {attribChanged: attribChanged};
  $.post('PICU-patients-add.php', data, function(data){
  // $(parent).html(data);
  location.reload();
});
}



$("textarea").each(function(textarea) {
    $(this).height( $(this)[0].scrollHeight );
});


  </script>


<script>
$(function() {
  $('input[name="admdate"]').daterangepicker({
    singleDatePicker: true,
    timePicker: true,
    timePicker24Hour: true,
    autoUpdateInput: false,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD  h:mm A'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
  var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    var gender = $(parent).find('.gender').find('select').val();
    var ht = $(parent).find('.ht').find('input').val();
    var wt = $(parent).find('.wt').find('input').val();
    var nationality = $(parent).find('.nationality').find('select').val();
    var birthdate = $(parent).find('.birthdate').find('input').val();
    var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var disdate = $(parent).find('.disdate').find('input').val();
    var disto = $(parent).find('.disto').find('select').val();
    var status = $(parent).find('.status').find('select').val();
    var comorbidities = $(parent).find('.comorbidities').find('select').val();
    
        var dischargediagnosis = $(parent).find('.dischargediagnosis').find('select').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
    var procedures = $(parent).find('.procedures').find('select').val();
    var complications = $(parent).find('.complications').find('select').val();

    var dnrbox = $(parent).find('.dnr').find('input[name$="dnr"]');
    if(dnrbox.prop('checked') === true){
          var dnr = $(parent).find('.dnr').find('input[name$="dnr"]').val();
        }else{
          var dnr = '';
        }

    var braindeathbox  = $(parent).find('.dnr').find('input[name$="braindeath"]');
    if(braindeathbox.prop('checked') === true){
          var braindeath = $(parent).find('.dnr').find('input[name$="braindeath"]').val();
        }else{
          var braindeath = '';
        }
      

var scotbox  = $(parent).find('.dnr').find('input[name$="scot"]');
    if(scotbox.prop('checked') === true){
          var scot = $(parent).find('.dnr').find('input[name$="scot"]').val();
        }else{
          var scot = '';
        }
      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {gender:gender,ht:ht,wt:wt, nationality:nationality, birthdate:birthdate, dnr:dnr, braindeath:braindeath, scot:scot,  admissiondiagnosis:admissiondiagnosis,dischargediagnosis:dischargediagnosis, status:status, disdate:disdate, disto: disto, id: id, bed: bed,admdate: admdate, admfrom: admfrom, mrn: mrn,name: name,comorbidities: comorbidities,procedures: procedures, complications: complications, attribChanged: attribChanged};
    $.post('PICU-patients-update-registry.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});

$(function() {
  $('input[name="birthdate"]').daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: false,
    showDropdowns: true,
    
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD'
        }
  }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
        var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    var gender = $(parent).find('.gender').find('select').val();
    var ht = $(parent).find('.ht').find('input').val();
    var wt = $(parent).find('.wt').find('input').val();
    var nationality = $(parent).find('.nationality').find('select').val();
    var birthdate = $(parent).find('.birthdate').find('input').val();
    var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var disdate = $(parent).find('.disdate').find('input').val();
    var disto = $(parent).find('.disto').find('select').val();
    var status = $(parent).find('.status').find('select').val();
    var comorbidities = $(parent).find('.comorbidities').find('select').val();
    
    var dischargediagnosis = $(parent).find('.dischargediagnosis').find('select').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
    var procedures = $(parent).find('.procedures').find('select').val();
    var complications = $(parent).find('.complications').find('select').val();

    var dnrbox = $(parent).find('.dnr').find('input[name$="dnr"]');
    if(dnrbox.prop('checked') === true){
          var dnr = $(parent).find('.dnr').find('input[name$="dnr"]').val();
        }else{
          var dnr = '';
        }

    var braindeathbox  = $(parent).find('.dnr').find('input[name$="braindeath"]');
    if(braindeathbox.prop('checked') === true){
          var braindeath = $(parent).find('.dnr').find('input[name$="braindeath"]').val();
        }else{
          var braindeath = '';
        }
      

var scotbox  = $(parent).find('.dnr').find('input[name$="scot"]');
    if(scotbox.prop('checked') === true){
          var scot = $(parent).find('.dnr').find('input[name$="scot"]').val();
        }else{
          var scot = '';
        }
      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {gender:gender,ht:ht,wt:wt, nationality:nationality, birthdate:birthdate, dnr:dnr, braindeath:braindeath, scot:scot,  admissiondiagnosis:admissiondiagnosis,dischargediagnosis:dischargediagnosis, status:status, disdate:disdate, disto: disto, id: id, bed: bed,admdate: admdate, admfrom: admfrom, mrn: mrn,name: name,comorbidities: comorbidities,procedures: procedures, complications: complications, attribChanged: attribChanged};
    $.post('PICU-patients-update-registry.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});
</script>

<script>
$(function() {
  $('input[name="disdate"]').daterangepicker({
    singleDatePicker: true,
    autoUpdateInput: false,
    timePicker: true,
    timePicker24Hour: true,
    showDropdowns: true,
    minYear: 2010,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
            format: 'YYYY-MM-DD  h:mm A'
        }
      }, ).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
        var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    var gender = $(parent).find('.gender').find('select').val();
    var ht = $(parent).find('.ht').find('input').val();
    var wt = $(parent).find('.wt').find('input').val();
    var nationality = $(parent).find('.nationality').find('select').val();
    var birthdate = $(parent).find('.birthdate').find('input').val();
    var admfrom = $(parent).find('.admfrom').find('select').val();
    var admdate = $(parent).find('.admdate').find('input').val();
    var disdate = $(parent).find('.disdate').find('input').val();
    var disto = $(parent).find('.disto').find('select').val();
    var status = $(parent).find('.status').find('select').val();
    var comorbidities = $(parent).find('.comorbidities').find('select').val();
    var dischargediagnosis = $(parent).find('.dischargediagnosis').find('select').val();
    var admissiondiagnosis = $(parent).find('.admissiondiagnosis').find('select').val();
    var procedures = $(parent).find('.procedures').find('select').val();
    var complications = $(parent).find('.complications').find('select').val();

    var dnrbox = $(parent).find('.dnr').find('input[name$="dnr"]');
    if(dnrbox.prop('checked') === true){
          var dnr = $(parent).find('.dnr').find('input[name$="dnr"]').val();
        }else{
          var dnr = '';
        }

    var braindeathbox  = $(parent).find('.dnr').find('input[name$="braindeath"]');
    if(braindeathbox.prop('checked') === true){
          var braindeath = $(parent).find('.dnr').find('input[name$="braindeath"]').val();
        }else{
          var braindeath = '';
        }
      

var scotbox  = $(parent).find('.dnr').find('input[name$="scot"]');
    if(scotbox.prop('checked') === true){
          var scot = $(parent).find('.dnr').find('input[name$="scot"]').val();
        }else{
          var scot = '';
        }
      
    //  alert (scot);

    var attribChanged = $(this).attr('name');
    data = {gender:gender,ht:ht,wt:wt, nationality:nationality, birthdate:birthdate, dnr:dnr, braindeath:braindeath, scot:scot,  admissiondiagnosis:admissiondiagnosis,dischargediagnosis:dischargediagnosis, status:status, disdate:disdate, disto: disto, id: id, bed: bed,admdate: admdate, admfrom: admfrom, mrn: mrn,name: name,comorbidities: comorbidities,procedures: procedures, complications: complications, attribChanged: attribChanged};
    $.post('PICU-patients-update-registry.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).parent('.eachcol').css("backgroundColor", "#90EE90");

  });
});




// var usedNames = {};
// $("select[name='procedures'] > option").each(function () {
//     if(usedNames[this.text]) {
//         $(this).remove();
//     } else {
//         usedNames[this.text] = this.value;
//     }
// });

</script>


<?php

require 'footer.php';

?>


