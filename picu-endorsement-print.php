<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
$date = $_GET['date'];
$date1 = $date;
if (!isset($date)){
  header("Location: PICU-Endorsement.php");
}
	require ('dbconnect.php');

  
  // get dates
$formationSQL = "SELECT Dates FROM endorsement";
$result = $mysqli->query($formationSQL);
$dates = $result -> fetch_all(MYSQLI_ASSOC);
$datesarray=array();
foreach ($dates as $d){
  array_push($datesarray,$d['Dates']);
}


$formationSQL = "SELECT * FROM endorsement WHERE Dates='".$date."'";
$result1 = $mysqli->query($formationSQL);
$endorsement = $result1 -> fetch_array(MYSQLI_ASSOC);

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  
 <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>PICU | Print Endorsement</title>

 
   <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
      <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    
  <!-- overlayScrollbars -->
  <!-- <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 
    <!-- Vendor CSS-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
	 <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"> </script>

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    input
{
    background-color: transparent !important;
    
}
body {
  background: rgb(204,204,204); 
  -webkit-print-color-adjust:exact;
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
@media print {
  body, page {
    margin: 0;
    size: landscape;
    box-shadow: 0;
    
  }
  @page {size: A4 landscape; }

  tr {page-break-inside: avoid;}
 .row{width:100%;}
}

</style>	
</head>

<page size="A4" layout="landscape">
  
<body  onload="window.print()" class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
 
  
	<?php


		
		$formationSQL = "SELECT * FROM patintsendorcement WHERE STAYDATE='".$date."'";
		$result1 = $mysqli->query($formationSQL);
		$patintsendorcement = $result1 -> fetch_all(MYSQLI_ASSOC);

		
function getdata($array, $key,$return, $val) {
    foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
            return $item[$return];
    return false;
}




			?>



    <!-- Main content -->

            <!-- /.info-box -->
         


                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
<?php 
 if (in_array($date, $datesarray, true)){
   ?>
                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                      
                        

                       <table>
                         <thead >
                         <tr><td colspan="5" style="padding-bottom: 10px;">
                         <div class="row">
                         <div class="col-md-3" style="text-align: center;">
                                   <img src="dist/img/EHC.jpg" style="width: 100%;">
                          </div>
                          <div class="col-md-6" style="text-align: center;">
                                      <p><span style='font-size:13.0pt;font-family:"Palatino Linotype",serif;
                                      '>QATIF HOSPITAL - PICU DEPARTMENT</span></p>

                                     <p><span
                                      style='font-size:13.0pt;font-family:"Palatino Linotype",serif;'>Daily Endorsement Sheet</span></p>

                                      <p>  <span
                                      style='font-size:11.0pt;font-family:"Palatino Linotype",serif;'>Consultant Covering: <?php echo $endorsement['consultantby']; ?></p> <p> 
                                       DAY:

                                      <?php
                                      date_default_timezone_set('Asia/Riyadh');
                                      $date=$date1;
                                      echo date('l', strtotime($date));
                                      ?>

                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DATE: <?php echo $date1; ?>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TIME: <?php echo $endorsement['time']; ?>
                                     </p>
                                      </span>
                                      </div>
                          <div class="col-md-3" style="text-align: center;">
                                  <img src="dist/img/QCN.jpg" style="width: 100%;">
                                  <img src="dist/img/qch1.png" style="width: 100%;">
                          </div>
                         </div>
                         </td></tr>
                         <tr style="border: solid 2px;text-align: center;background: gainsboro; font-weight: bold">
                          <td   style='border-right: solid 0.5px;padding: 4px; width: 8%;'>Info</td>
                          <td  style='border-right: solid 0.5px;padding: 4px; width: 20%;'>Diagnosis List</td>
                          <td  style='border-right: solid 0.5px;padding: 4px; width: 37%;'>Clinical Condition</td>
                          <td  style='border-right: solid 0.5px;padding: 4px; width: 20%;'>Plan Of Care</td>
                          <td  style='border-right: solid 0.5px;padding: 4px; width: 15%;'>Night Event</td>
                          
                          </tr>
                      </thead> 
                  <?php
function cmp($row1, $row2) {
  // $row1[0] is the item in your first array, etc
  if($row1['BED'] == $row2['BED']) {
      return 0;
  }
  else if($row1['BED'] == '') {
      return 1;
  }
  else if($row2['BED'] == '') {
      return -1;
  }

  return $row1['BED'] < $row2['BED'] ? -1 : 1;
}

usort($patintsendorcement, "cmp");

                 
            foreach($patintsendorcement as $s){

                echo "

               
    
      <tr style='border: solid 2px;padding: 2px;'>
        <td style='border-right: solid 0.5px;padding: 4px;'>
        <p>".$s['PNAME']."</p>
        <p><strong>Bed:</strong> ".$s['BED']."</p>
        <p><strong>MRN:</strong> ".$s['MRN']."</p>
        
        </td>
        <td style='border-right: solid 0.5px;padding: 4px;'>
        ".nl2br($s['DISEASE'])."
        </td>
        <td style='border-right: solid 0.5px;padding: 4px;'>
        ".nl2br($s['DETAILS'])."
        </td>
        <td style='border-right: solid 0.5px;padding: 4px;'>
        <p>".nl2br($s['PLAN'])."</p>
        </td>
        <td style='border-right: solid 0.5px;padding: 4px;'>
        <p>".nl2br($s['nevent'])."</p>
        </td>
      </tr>
   

    ";

            }
            // 

            ?>

</table>
<?php 
 } else{

  echo "<a style='font-weight: bold;color: red;'>Choose another date or contact administrator if the date is correct</a>";

 }

// print_r ($endorsement);
   ?>


                        <div class="row" style=" margin-top: 2%; ">
                            <div class="col-sm-6" id="endby">
                              
                            
                                    <?php
                                    if (!empty($endorsement['endorsedby'])){

                                    $formationSQL = "SELECT * FROM members WHERE member_id='".$endorsement['endorsedby']."'";
                                    $result1 = $mysqli->query($formationSQL);
                                    $endorsedby = $result1 -> fetch_array(MYSQLI_ASSOC);


                                    echo '<p><strong>Endorsed By: </strong> '. $endorsedby['full_name']. '</p>';
                                    }else{
                                    echo '<p><strong>Endorsed By: </strong> Not Selected</p>';
                                    }
                                    ?>

                            </div>
                            <div class=" col-sm-6" id="endto">
                              
                           
                                   
   
                                    <?php
                                    if (!empty($endorsement['endorsedto'])){

                                    $formationSQL = "SELECT * FROM members WHERE member_id='".$endorsement['endorsedto']."'";
                                    $result1 = $mysqli->query($formationSQL);
                                    $endorsedto = $result1 -> fetch_array(MYSQLI_ASSOC);


                                    echo '<p><strong>Endorsed To: </strong>'. $endorsedto['full_name']. '</p>';
                                    }else{
                                    echo '<p><strong>Endorsed By: </strong> Not Selected</p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                      

                 

</div>
   


                  
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
    
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             


           
 </div> <!--row -->


  </body>
</page>
</html>
