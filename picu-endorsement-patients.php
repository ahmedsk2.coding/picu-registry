<?php 
session_start();

require_once "authCookieSessionValidate.php";

if(!$isLoggedIn) {
    header("Location: ./");
}
$date = $_GET['date'];
$date1 = $date;
if (!isset($date)){
  header("Location: PICU-Endorsement.php");
}




?>

  <!-- Navbar -->
<?php
require 'sidebar.php';
	require ('dbconnect.php');

  if (!in_array($user['position'],$access_PICU_endorsement)){
    
    echo "
    <div class='content-wrapper'>
    
  
    <section class='content'>
    <div class='container-fluid'>  
    <div class='alert alert-danger' role='alert'> you dont have permission to access this page, Contact you manager if you need to.
    </div>
    </div>
    </section>
    </div>
    ";
    require 'footer.php';

    exit();
  }
  
  // get dates
$formationSQL = "SELECT Dates FROM endorsement";
$result = $mysqli->query($formationSQL);
$dates = $result -> fetch_all(MYSQLI_ASSOC);
$datesarray=array();
foreach ($dates as $d){
  array_push($datesarray,$d['Dates']);
}
?>


     <style>
          @media print{@page {size: landscape;  margin: 2%;}
            body *{
              visibility: hidden;
              -webkit-print-color-adjust:exact;
            }
           
            #mypresentersTable * {
              visibility: visible;
            }
            #mypresentersTable {
              position: absolute;
              left: 0;
              top: -50px;
            }
            #addbtn * {
              visibility: hidden;
            }
            #btns * {
              visibility: hidden;
            }
            .card-header{
              background-color: rgba(0,0,0,.03);
            }
        }
          .hidden{
                  display: none;
          }
          textarea {
    resize: none;
    overflow: hidden;
}

      </style>

        <script>

function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}

function calc(value) {

  var rowname= "row";
  rowname+=value;
  row = document.getElementById(rowname);
    var id = value;
    data = {id: id};
    $.post('update-PICU-Endorsement-code-delete.php', data, function(data){
    // $(parent).html(data);
  });
    row.style.display = "none";
    }
 

	</script>
      
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
	<?php

// genereate random unique number

function generateRandomString($length = 8) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
		

		
		$formationSQL = "SELECT * FROM patintsendorcement WHERE STAYDATE='".$date."'";
		$result1 = $mysqli->query($formationSQL);
		$patintsendorcement = $result1 -> fetch_all(MYSQLI_ASSOC);

		
function getdata($array, $key,$return, $val) {
    foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
            return $item[$return];
    return false;
}




			?>



    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">PICU Endorsement</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">PICU Endorsement for <?php echo $date; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">  
      

<div class="row">

 <div id="mypresentersTable" class="col-md-12">

            <!-- /.info-box -->

            <div class="card">
              <div  class="card-header">
                <h3 class="card-title"><i class="fas fa-user-tie text-info"></i> PICU Endorsement for <?php echo $date; ?></h3>
                <div id="btns">
                            <!-- <a class="btn btn--radius-2 btn--blue"  style="color: aliceblue; line-height: 2;padding: 0px 15px;"  name="endorsebtn" id="endorsebtn" value="submit">Complete Endorsement</a> -->
                            <a target="_blank" style="float: right;font-weight: 900;" href='picu-endorsement-print.php?date=<?php echo $date ;?>'><i class="fas fa-print"></i>  Print this page</a>
                        </div>
                <div class="card-tools">
					
                  <!-- <button type="button" class=" btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class=" btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button> -->
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
<?php 
 if (in_array($date, $datesarray, true)){
   ?>
                    
                       <!-- <input type="text" class="mytablesearchInput" id="mypresenterssearchInput" onkeyup="mypresentersTable()" placeholder="Search for speaker names.." title="Type in a speaker name"> -->
                                      
                        


                  <?php
function cmp($row1, $row2) {
  // $row1[0] is the item in your first array, etc
  if($row1['BED'] == $row2['BED']) {
      return 0;
  }
  else if($row1['BED'] == '') {
      return 1;
  }
  else if($row2['BED'] == '') {
      return -1;
  }

  return $row1['BED'] < $row2['BED'] ? -1 : 1;
}

usort($patintsendorcement, "cmp");

                 
            foreach($patintsendorcement as $s){

                echo "
                <div class='row'>
                
                <div class='eachrow row' style='margin-top: 0px; margin-right: 0px; margin-bottom: 1%; margin-left: 0px;' id='row".$s['ID']."'>
                <div class='eachcol id' scope='row' ><input class='txtdata' name='id' value='".$s['ID']."' style='display: none;'>
                </div>
                <div  style=' display: flex;' class='eachcol card-header col-sm-1'  scope='row' >
                <a  type='button' class='btn-tool'  style=' margin: auto; font-size: large; '   onclick='calc(".$s['ID'].")'><i class='fa fa-trash'></i></a>
          
                </div>

      <div  style='' class='eachcol bed card-header col-sm-3'  scope='row' >

      <label style='margin: 0px; text-align: center; '>Bed Number</label>
      <input class='txtdata' placeholder='Bed Number' name='bed' value='".$s['BED']."'>

      </div>
      

      <div style=' ; ' class='eachcol mrn card-header col-sm-4' >
      <label style='margin: 0px; text-align: center; '>MRN</label>
      <input class='txtdata' name='mrn' value='".$s['MRN']."' ></div>

      <div style=' ; ' class='eachcol name card-header col-sm-4'>
      <label style='margin: 0px; text-align: center; '>Patient Name</label>
      <input class='txtdata' name='name' value='".$s['PNAME']."' ></div>

      <div style=' ; ' class='eachcol disease col-sm-3'>
      <label style='margin: 0px; text-align: center; '>Problem List</label>
      <textarea  oninput='auto_grow(this)' class='txtdata' name='disease' value='".$s['DISEASE']."' >".$s['DISEASE']."</textarea></div>
      
  
   <div style=' ; ' class='eachcol details col-sm-4'>
   <label style='margin: 0px; text-align: center; '>Clinical Condition</label>
   <textarea   oninput='auto_grow(this)' class='txtdata' name='details' value='".$s['DETAILS']."' >".$s['DETAILS']."</textarea></div>
   
   <div style=' ; ' class='eachcol plan col-sm-3'>
   <label style='margin: 0px; text-align: center; '>Plan of Care</label>
   <textarea   oninput='auto_grow(this)' class='txtdata' name='plan' value='".$s['PLAN']."' >".$s['PLAN']."</textarea></div>

   <div style=' ; ' class='eachcol nevent col-sm-2'>
   <label style='margin: 0px; text-align: center; '>Night Events</label>
   <textarea   oninput='auto_grow(this)' class='txtdata' name='nevent' value='".$s['nevent']."' >".$s['nevent']."</textarea></div>
    </div >
    </div >
    ";

            }
            // 

            ?>

        
<div id="addbtn" class='eachrow'>
                <td class='eachcol id' scope='row' colspan='6'>
                <a  type='button' class="btn btn-success"  style="color: aliceblue; line-height: 2;padding: 0px 15px;"  id= 'addpatient'> Add New Patient</a></td>
          </div>
<?php 
 } else{

  echo "<a style='font-weight: bold;color: red;'>Choose another date or contact administrator if the date is correct</a>";

 }

 $formationSQL = "SELECT * FROM endorsement WHERE Dates='".$date."'";
 $result1 = $mysqli->query($formationSQL);
 $endorsement = $result1 -> fetch_array(MYSQLI_ASSOC);

// print_r ($endorsement);
   ?>


<form  id="addendorsers">

<div class="row" style=" margin-top: 2%; ">
                            <div class="col-sm-6" id="conby">
                                <div class="input-group" style=' margin: 0px; '>
                                    <label class="label col " >Consultant Endorsing</label>
                                    
                                    <input class="col-sm-6 conby" type="text" value="<?php echo $endorsement['consultantby'];?>" id="consultantby" required>
                                    
                                </div>
                            </div>
                            <div class=" col-sm-6" id="conto">
                                <div class="input-group " style=' margin: 0px; '>
                                <label class="label col " >Consultant Receiving</label>
                                <input class="col-sm-6 conto" type="text" value="<?php echo $endorsement['consultantto'];?>" id="consultantto" required>
										</div>
                                </div>
                            </div>
                      

                        <div class="row" style=" margin-top: 2%; ">
                            <div class="col-sm-6" id="endby">
                                <div class="input-group" style=' margin: 0px; '>
                                    <label class="label col " >Endorsed By</label>
                                     <!-- <input class="input--style-4 col-9" type="text" value="<?php echo $endorsement['endorsedby'];?>" id="endorsedby" required> -->
                                    <?php echo"

                                    <select class='select2 col endby' name='endorsedby' id='endorsedby'  >
      ";


  $formationSQL = "SELECT * FROM members WHERE position='4'";
  $result1 = $mysqli->query($formationSQL);
  $residents = $result1 -> fetch_all(MYSQLI_ASSOC);
    // $selected = in_array($key, $decodedP) ? 'selected ' : '';

if (!empty($endorsement['endorsedby'])){

$formationSQL = "SELECT * FROM members WHERE member_id='".$endorsement['endorsedby']."'";
  $result1 = $mysqli->query($formationSQL);
  $endorsedby = $result1 -> fetch_array(MYSQLI_ASSOC);


  echo '<option selected value="'.$endorsedby['member_id'].'">'. $endorsedby['full_name']. '</option>';
}else{
  echo '<option disabeled value="">Select</option>';
}

  foreach($residents as $residentslist){
    echo"<option value='".$residentslist['member_id']."'>".$residentslist['full_name']."</option>";
  }
    echo"  
    </select>
    ";
    ?>
                                </div>
                            </div>
                            <div class=" col-sm-6" id="endto">
                                <div class="input-group " style=' margin: 0px; '>
                                <label class="label col " >Endorsed To</label>
                                    <!-- <input class="input--style-4 col-9"  type="text" value="<?php echo $endorsement['endorsedto'];?>" id="endorsedto" required> -->
                                     <?php echo"

                                    <select class='select2 col endto' name='endorsedto' id='endorsedto'  >
      ";


if (!empty($endorsement['endorsedto'])){

$formationSQL = "SELECT * FROM members WHERE member_id='".$endorsement['endorsedto']."'";
  $result1 = $mysqli->query($formationSQL);
  $endorsedto = $result1 -> fetch_array(MYSQLI_ASSOC);


  echo '<option selected value="'.$endorsedto['member_id'].'">'. $endorsedto['full_name']. '</option>';
}else{
  echo '<option disabeled value="">Select</option>';
}

  foreach($residents as $residentslist){
    echo"<option value='".$residentslist['member_id']."'>".$residentslist['full_name']."</option>";
  }
    echo"  
    </select>
    ";
    ?>
										</div>
                                </div>
                            </div>
                   

                            <div class="row" style=" margin-top: 2%; ">
                            <div class="col-sm-6" id="time">
                                <div class="input-group" style=' margin: 0px; '>
                                    <label class="label col " >Endorsement At:</label>
                                   
                                    <?php echo"

                                    <select class='select2 col time' name='timing' id='timing'  >
      ";


if (!empty($endorsement['time'])){

  echo '<option selected value="'.$endorsement['time'].'">'. $endorsement['time']. '</option>
  <option  value="7:30 Am">7:30 Am</option>
  <option  value="13:30">13:30</option>';
  
}else{
  echo '<option disabeled value="">Select</option>
  <option  value="7:30 Am">7:30 Am</option>
  <option  value="13:30">13:30</option>'
  
  
  
  ;
}

    echo"  
    </select>
    ";
    ?>
                                </div>
                            </div>
                            
                            </div>
                   


                        <div class="p-t-15 mssssg" id="btns">
                            <!-- <a class="btn btn--radius-2 btn--blue"  style="color: aliceblue; line-height: 2;padding: 0px 15px;"  name="endorsebtn" id="endorsebtn" value="submit">Complete Endorsement</a> -->
                            <a target="_blank" style="font-weight: 900;" href='picu-endorsement-print.php?date=<?php echo $date ;?>'><i class="fas fa-print"></i>  Print this page</a>
                        </div>
                    </form>

  
                    
                    
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
             
              <!-- /.footer -->
            </div>
            <!-- /.card -->
</div>
			

           
 </div> <!--row -->
			
 

<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

<!-- PAGE SCRIPTS -->


<?php
	



?>
</div><!--/. container-fluid -->

    </section>
    <!-- /.content -->
    


  </div>
  <!-- /.content-wrapper -->

<?php

require 'footer.php';

?>



<!-- <script>
function mypresentersTable() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("mypresenterssearchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("mypresentersTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script> -->

<script>
$(document).ready(function($) {

  


  $('.txtdata').on('change', function(){
    var parent = $(this).parent('.eachcol').parent('.eachrow');
    var id = $(parent).find('.id').find('input').val();
    var bed = $(parent).find('.bed').find('input').val();
    var mrn = $(parent).find('.mrn').find('input').val();
    var name = $(parent).find('.name').find('input').val();
    var disease = $(parent).find('.disease').find('textarea').val();
    var details = $(parent).find('.details').find('textarea').val();
    var plan = $(parent).find('.plan').find('textarea').val();
    var nevent = $(parent).find('.nevent').find('textarea').val();
    var date = new Date("<?php echo $date1; ?>");
    var date1 = date.toLocaleDateString();

    var attribChanged = $(this).attr('name');
    data = {id: id, name: name, bed: bed,mrn: mrn,name: name,disease: disease,details: details, plan:plan,nevent:nevent, date1: date1, attribChanged: attribChanged};
    $.post('update-PICU-Endorsement-code.php', data, function(data){
      // $(parent).html(data);
     
    });
    $(this).css("backgroundColor", "#90EE90");
  });
});
</script>
<script>

document.getElementById('addpatient').onclick = function(){
  var parent = $(this).parent('.eachcol').parent('.eachrow');
  var attribChanged = $(this).attr('name');
  var date = new Date("<?php echo $date1; ?>");
  var date1 = date.toLocaleDateString();
  data = {attribChanged: attribChanged, date1: date1};
  $.post('update-PICU-Endorsement-code-add.php', data, function(data){
  // $(parent).html(data);
  location.reload();
});
}


$(document).ready(function($) {
  
  $('.time').on('change', function(){
    var parent = $(this).parent('.mssssg');
    time=document.getElementById('timing').value;
    consultantto=document.getElementById('consultantto').value;
  consultantby=document.getElementById('consultantby').value;

  // alert(endorsedby);
  endorsedby=document.getElementById('endorsedby').value;
  endorsedto=document.getElementById('endorsedto').value;
  var date = new Date("<?php echo $date1; ?>");
  var date1 = date.toLocaleDateString();
  data = {endorsedby: endorsedby, endorsedto: endorsedto,consultantby:consultantby,consultantto:consultantto,time:time, date1: date1};
  $.post('validate-endorsement.php', data, function(data){
  $(parent).html(data);
  document.getElementById('time').style.backgroundColor = "#90EE90";

  // location.reload();
}
  );
  });

  $('.endby').on('change', function(){
    var parent = $(this).parent('.mssssg');
    time=document.getElementById('timing').value;
    consultantto=document.getElementById('consultantto').value;
  consultantby=document.getElementById('consultantby').value;

  // alert(endorsedby);
  endorsedby=document.getElementById('endorsedby').value;
  endorsedto=document.getElementById('endorsedto').value;
  var date = new Date("<?php echo $date1; ?>");
  var date1 = date.toLocaleDateString();
  data = {endorsedby: endorsedby, endorsedto: endorsedto,consultantby:consultantby,consultantto:consultantto,time:time, date1: date1};
  $.post('validate-endorsement.php', data, function(data){
  $(parent).html(data);
  document.getElementById('endby').style.backgroundColor = "#90EE90";

  // location.reload();
}
  );
  });

  $('.endto').on('change', function(){
    var parent = $(this).parent('.mssssg');
    time=document.getElementById('timing').value;
    consultantto=document.getElementById('consultantto').value;
  consultantby=document.getElementById('consultantby').value;

  // alert(endorsedby);
  endorsedby=document.getElementById('endorsedby').value;
  endorsedto=document.getElementById('endorsedto').value;
  var date = new Date("<?php echo $date1; ?>");
  var date1 = date.toLocaleDateString();
  data = {endorsedby: endorsedby, endorsedto: endorsedto,consultantby:consultantby,consultantto:consultantto,time:time, date1: date1};
  $.post('validate-endorsement.php', data, function(data){
  $(parent).html(data);
  document.getElementById('endto').style.backgroundColor = "#90EE90";
  // location.reload();
}
  );
  });

  $('.conby').on('change', function(){
    var parent = $(this).parent('.mssssg');
    time=document.getElementById('timing').value;
    consultantto=document.getElementById('consultantto').value;
  consultantby=document.getElementById('consultantby').value;

  // alert(endorsedby);
  endorsedby=document.getElementById('endorsedby').value;
  endorsedto=document.getElementById('endorsedto').value;
  var date = new Date("<?php echo $date1; ?>");
  var date1 = date.toLocaleDateString();
  data = {endorsedby: endorsedby, endorsedto: endorsedto,consultantby:consultantby,consultantto:consultantto,time:time, date1: date1};
  $.post('validate-endorsement.php', data, function(data){
  $(parent).html(data);
  document.getElementById('conby').style.backgroundColor = "#90EE90";
  // location.reload();
}
  );
  });
  $('.conto').on('change', function(){
    var parent = $(this).parent('.mssssg');
    time=document.getElementById('timing').value;
    consultantto=document.getElementById('consultantto').value;
  consultantby=document.getElementById('consultantby').value;

  // alert(endorsedby);
  endorsedby=document.getElementById('endorsedby').value;
  endorsedto=document.getElementById('endorsedto').value;
  var date = new Date("<?php echo $date1; ?>");
  var date1 = date.toLocaleDateString();
  data = {endorsedby: endorsedby, endorsedto: endorsedto,consultantby:consultantby,consultantto:consultantto,time:time, date1: date1};
  $.post('validate-endorsement.php', data, function(data){
  $(parent).html(data);
  document.getElementById('conto').style.backgroundColor = "#90EE90";
  // location.reload();
}
  );
  });
});

// document.getElementById('endorsebtn').onclick = function(){
//   var parent = $(this).parent('.mssssg');
//   endorsedby=document.getElementById('endorsedby').value;
//   // alert(endorsedby);
//   endorsedto=document.getElementById('endorsedto').value;
//   var date = new Date("<?php //echo $date1; ?>");
//   var date1 = date.toLocaleDateString();
//   data = {endorsedby: endorsedby, endorsedto: endorsedto, date1: date1};
//   $.post('validate-endorsement.php', data, function(data){
//   $(parent).html(data);
//   // location.reload();
// }
// );

// }
function printExternal(url) {
    var printWindow = window.open( url, 'Print', 'left=200, width=950, height=500, toolbar=0, resizable=0');

    printWindow.addEventListener('load', function() {
        if (Boolean(printWindow.chrome)) {
            printWindow.print();
            setTimeout(function(){
                printWindow.close();
            }, 500);
        } else {
            printWindow.print();
            printWindow.close();
        }
    }, true);
}


$("textarea").each(function(textarea) {
    $(this).height( $(this)[0].scrollHeight );
});

 $(document).ready(function() {
        $('.select2').select2({
      placeholder: 'Select',
      } );
 });

 jQuery(document).bind("keyup keydown", function(e){
    if(e.ctrlKey && e.keyCode == 80){
        window.open('picu-endorsement-print.php?date=<?php echo $date ;?>', '_blank');
        return false;
     
    }
    
});
  </script>


